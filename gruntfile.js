module.exports = function (grunt) {
    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        uglify: {
            options: {
                mangle: true,
                compress: {
                    sequences: false
                }
            },
            build: {
                src: [
                    'wp-content/themes/harman/assets/js/src/*.js',
                ],
                dest: 'wp-content/themes/harman/assets/js/main.min.js'
            }
        },
        concat: {
            basic: {
                src: [
                    'wp-content/themes/harman/assets/css/src/*.less',
                ],
                dest: 'wp-content/themes/harman/assets/css/staging.less'
            }
        },
        less: {
            options: {
                compress: true,
                banner: '@charset "UTF-8";'
            },
            build: {
                src: 'wp-content/themes/harman/assets/css/staging.less',
                dest: 'wp-content/themes/harman/assets/css/style.min.css'
            }
        }
    });

    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-less');

    // Default task(s).
    grunt.registerTask('default', ['uglify', 'concat', 'less']);
    grunt.registerTask('dev-js', ['uglify']);
    grunt.registerTask('dev-less', ['concat', 'less']);
};
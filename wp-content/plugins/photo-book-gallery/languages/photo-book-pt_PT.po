msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: \n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=iso-8859-1\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.11\n"
"Language: pt_PT\n"

#: class.main.php:33 class.main.php:44
msgid "Photo Books"
msgstr "Livros de fotografias"

#: class.main.php:34 plugin.class.php:76 plugin.class.php:164
msgid "Photo Book"
msgstr "Livro de Fotos"

#: class.main.php:36 plugin.class.php:156 plugin.class.php:239
msgid "Add New Book"
msgstr "Adicionar novo livro"

#: class.main.php:37
msgid "Edit Book"
msgstr "Editar livro"

#: class.main.php:38
msgid "New Book"
msgstr "Livro novo"

#: class.main.php:39
msgid "View Photo Book"
msgstr "Ver Livro de Fotos"

#: class.main.php:40
msgid "Search Books"
msgstr "Pesquisar Livros"

#: class.main.php:41
msgid "No Photo Books found"
msgstr "Nenhum foto Livros encontrados"

#: class.main.php:42
msgid "No Photo Books found in Trash"
msgstr "Nenhum livro de fotos encontrado na lixeira"

#: class.main.php:43
msgid "Parent Book:"
msgstr "Livro pai:"

#: class.main.php:85
msgid "Images"
msgstr "Imagens"

#: class.main.php:87
msgid "Settings"
msgstr "Configurações"

#: class.main.php:89
msgid "Help and Support"
msgstr "Ajuda e suporte"

#: class.main.php:95
msgid "Use following shortcode in text section of editor."
msgstr "Use o seguinte código abreviado na seção de texto do editor."

#: class.main.php:105
msgid "Having trouble? Book is not working properly?"
msgstr "Está com problemas? Livro não está funcionando corretamente?"

#: class.main.php:106
msgid "How to use"
msgstr "Como usar"

#: class.main.php:107
msgid "contact us for help"
msgstr "Entre em contato conosco para obter ajuda"

#: class.main.php:111
msgid "Why Upgrade to Pro?"
msgstr "Por que atualizar para Pro?"

#: class.main.php:113
msgid ""
"In pro version you will be able to add page title and chapter title with "
"each page."
msgstr ""
"Na versão pro você poderá adicionar o título da página eo título do capítulo "
"a cada página."

#: class.main.php:114
msgid "By upgrading to pro, your existing books will remain saved."
msgstr "Ao atualizar para o pro, seus livros existentes permanecerão salvos."

#: class.main.php:117
msgid "Unlock Pro Features"
msgstr "Desbloquear recursos do Pro"

#: class.main.php:120
msgid "Visual Composer Addon"
msgstr "Addon Visual Composer"

#: class.main.php:122
msgid "Photo Book is now available as visual composer extension"
msgstr "O Photo Book está agora disponível como extensão visual do compositor"

#: class.main.php:123
msgid "Get Here"
msgstr "Obtenha aqui"

#: class.main.php:126
msgid "Real 3D Flip Book"
msgstr "Livro de Flip 3D real"

#: class.main.php:128
msgid "We have developed a new plugin with real 3d effect and touch support"
msgstr "Desenvolvemos um novo plugin com efeito real 3d e suporte ao toque"

#: class.main.php:129
msgid "Download FREE"
msgstr "Download grátis"

#: class.main.php:163
msgid "Looking for Real 3D Book?"
msgstr "Procurando Real 3D Book?"

#: class.main.php:166
msgid "Download Real 3D Flip Book"
msgstr "Baixar Real 3D Flip Book"

#: class.main.php:291 class.main.php:312 class.main.php:343 class.main.php:360
msgid "Default"
msgstr "Padrão"

#: class.main.php:298 class.main.php:319 class.main.php:367
msgid "Pro Feature"
msgstr "Recurso Pro"

#: class.main.php:357 plugin.class.php:111 plugin.class.php:117
#: plugin.class.php:124 plugin.class.php:130 plugin.class.php:132
#: plugin.class.php:197 plugin.class.php:204 plugin.class.php:210
#: plugin.class.php:212 plugin.class.php:224
msgid "Enable"
msgstr "Habilitar"

#: plugin.class.php:62
msgid "Photo Book Gallery Settings"
msgstr "Definições da galeria de livros de fotografias"

#: plugin.class.php:65
msgid "Recommended"
msgstr "Recomendado"

#: plugin.class.php:65
msgid "Please use new system to create Photo Books"
msgstr "Utilize um novo sistema para criar livros fotográficos"

#: plugin.class.php:66
msgid "HERE"
msgstr "AQUI"

#: plugin.class.php:69
msgid "Dismiss this notice"
msgstr "Descartar essa notificação"

#: plugin.class.php:78 plugin.class.php:166 settings_fields.php:6
msgid "General Settings"
msgstr "Configurações Gerais"

#: plugin.class.php:81 plugin.class.php:169 settings_fields.php:12
msgid "Book Width"
msgstr "Largura do livro"

#: plugin.class.php:84 plugin.class.php:88 plugin.class.php:171
#: plugin.class.php:174
msgid "Leave Blank for Responsive"
msgstr "Deixe em branco para responder"

#: plugin.class.php:86 plugin.class.php:172 settings_fields.php:20
msgid "Book Height"
msgstr "Altura do livro"

#: plugin.class.php:91 plugin.class.php:177 settings_fields.php:28
msgid "Speed"
msgstr "Velocidade"

#: plugin.class.php:93 plugin.class.php:179 settings_fields.php:48
msgid "Starting Page"
msgstr "Página inicial"

#: plugin.class.php:97 plugin.class.php:183 settings_fields.php:36
msgid "Reading Direction"
msgstr "Direção de Leitura"

#: plugin.class.php:100 plugin.class.php:186 settings_fields.php:38
msgid "Right to Left"
msgstr "Direita para esquerda"

#: plugin.class.php:101 plugin.class.php:187 settings_fields.php:39
msgid "Left to Right"
msgstr "Da esquerda para direita"

#: plugin.class.php:104 plugin.class.php:190 settings_fields.php:116
msgid "Page Padding"
msgstr "Remoção de página"

#: plugin.class.php:108 plugin.class.php:194 settings_fields.php:124
msgid "Page Numbers"
msgstr "Números de página"

#: plugin.class.php:109 plugin.class.php:136 plugin.class.php:138
#: plugin.class.php:195 plugin.class.php:216 plugin.class.php:218
msgid "Show"
msgstr "exposição"

#: plugin.class.php:110 plugin.class.php:196 settings_fields.php:86
msgid "Closed Book"
msgstr "Livro fechado"

#: plugin.class.php:114 plugin.class.php:221
msgid "Book Title (for your reference)"
msgstr "Título do livro (para sua referência)"

#: plugin.class.php:116 plugin.class.php:223
msgid "Zoom on Hover"
msgstr "Aumentar o zoom sobre o mouse"

#: plugin.class.php:120 plugin.class.php:200 settings_fields.php:140
msgid "Controls"
msgstr "Controles"

#: plugin.class.php:123 plugin.class.php:203
msgid "AutoPlay"
msgstr "Reprodução automática"

#: plugin.class.php:125 plugin.class.php:205
msgid "AutoPlay delay for each page (in ms)"
msgstr "Retardo de Reprodução Automática para cada página (em ms)"

#: plugin.class.php:129
msgid "Turn Page by clicking Image"
msgstr "Ativar página clicando em Imagem"

#: plugin.class.php:131 plugin.class.php:211 settings_fields.php:298
msgid "Keyboard Controls"
msgstr "Controles de Teclado"

#: plugin.class.php:135 plugin.class.php:215 settings_fields.php:202
msgid "Navigation Tabs"
msgstr "Guias de Navegação"

#: plugin.class.php:137 plugin.class.php:217 settings_fields.php:258
msgid "Arrows"
msgstr "Setas"

#: plugin.class.php:155 plugin.class.php:238
msgid "Delete"
msgstr "Excluir"

#: plugin.class.php:157 plugin.class.php:240 render_box_contents.php:4
msgid "Upload Images"
msgstr "Enviar imagens"

#: plugin.class.php:159 plugin.class.php:242
msgid "Shortcode"
msgstr "Código curto"

#: plugin.class.php:209
msgid "Turn Page by clicking anywhere"
msgstr "Ativar página clicando em qualquer lugar"

#: plugin.class.php:249
msgid "Save Changes"
msgstr "Salvar alterações"

#: plugin.class.php:251
msgid "Changes Saved"
msgstr "Mudanças salvas"

#: settings_fields.php:13
msgid "Provide width for closed book in pixels, leave blank for responsive"
msgstr ""
"Fornecer largura para o livro fechado em pixels, deixar em branco para "
"resposta"

#: settings_fields.php:21
msgid "Provide height for book in pixels, leave blank for responsive"
msgstr "Fornecer altura para o livro em pixels, deixar em branco para resposta"

#: settings_fields.php:29
msgid "Provide speed of page turn in milliseconds"
msgstr "Fornecer velocidade de rotação da página em milissegundos"

#: settings_fields.php:41
msgid "Choose reading direction for pages"
msgstr "Escolha direção de leitura para páginas"

#: settings_fields.php:49
msgid "Provide starting page for the book"
msgstr "Forneça a página inicial do livro"

#: settings_fields.php:56
msgid "Easing"
msgstr "Facilidade"

#: settings_fields.php:57
msgid "Easing method for the complete page transition"
msgstr "Método de atenuação para a transição de página completa"

#: settings_fields.php:64
msgid "Ease In"
msgstr "Facilidade"

#: settings_fields.php:65
msgid "Easing method for the first half of the page transition"
msgstr "Método de atenuação para a primeira metade da transição da página"

#: settings_fields.php:72
msgid "Ease Out"
msgstr "Aliviar"

#: settings_fields.php:73
msgid "Easing method for the second half of the page transition"
msgstr "Método de atenuação para a segunda metade da transição da página"

#: settings_fields.php:80
msgid "Closed Book and Covers"
msgstr "Livro fechado e capas"

#: settings_fields.php:87
msgid "Enable it to give the book the appearance of being closed"
msgstr "Permitir que ele dê ao livro a aparência de ser fechado"

#: settings_fields.php:94
msgid "Covers"
msgstr "Capas"

#: settings_fields.php:95
msgid "Makes the first and last pages into covers by hiding page numbers"
msgstr ""
"Torna a primeira e a última páginas em capas ocultando números de página"

#: settings_fields.php:102
msgid "Auto Center"
msgstr "Auto Center"

#: settings_fields.php:103
msgid "Makes the book position in the center of its container when closed"
msgstr "Faz a posição do livro no centro do seu recipiente quando fechado"

#: settings_fields.php:110
msgid "Pages"
msgstr "Páginas"

#: settings_fields.php:117
msgid "Provide inner space for pages in pixels"
msgstr "Fornecer espaço interno para páginas em pixels"

#: settings_fields.php:125
msgid "Check to display page numbers"
msgstr "Verificar para exibir números de página"

#: settings_fields.php:132
msgid "Page Border"
msgstr "Página Limite"

#: settings_fields.php:133
msgid "The size of the border around each page"
msgstr "O tamanho da borda ao redor de cada página"

#: settings_fields.php:146
msgid "Manual Control"
msgstr "Controle manual"

#: settings_fields.php:147
msgid "Enables manual page turning using click and drag"
msgstr "Permite a rotação manual de páginas usando o clique e arraste"

#: settings_fields.php:154
msgid "Hovers"
msgstr "Paira"

#: settings_fields.php:155
msgid "Shows a small preview of page turn on hover"
msgstr "Mostra uma pequena visualização da página ativar o hover"

#: settings_fields.php:162
msgid "Hover Width"
msgstr "Largura do cursor"

#: settings_fields.php:163
msgid "The width of the page turn hover preview in pixels"
msgstr "A largura da página virar visualização em pixels"

#: settings_fields.php:170
msgid "Hover Speed"
msgstr "Velocidade do cursor"

#: settings_fields.php:171
msgid "The speed in milliseconds of the page turn hover preview"
msgstr "A velocidade em milissegundos da página"

#: settings_fields.php:178
msgid "Hover Treshold"
msgstr "Limiar de Hover"

#: settings_fields.php:179
msgid "The percentage used with manual page dragging"
msgstr "A porcentagem usada com o arrastar de página manual"

#: settings_fields.php:186
msgid "Hover Click"
msgstr "Clique em Hover"

#: settings_fields.php:187
msgid ""
"Enables a \"click\" on the page turn hover preview, when using manual page "
"turning, to begin the page turn"
msgstr ""
"Habilita um \"clique\" na visualização de hover de turno de página, ao usar "
"a virada de página manual, para iniciar a página virar"

#: settings_fields.php:194
msgid "Overlays"
msgstr "Sobreposições"

#: settings_fields.php:195
msgid ""
"Enables navigation using a page sized overlay. When enabled page content, "
"like links, will not be clickable. If manual option is enabled, overlays are "
"removed"
msgstr ""
"Ativa a navegação usando uma sobreposição de tamanho de página. Quando o "
"conteúdo da página ativado, como links, não será clicável. Se a opção manual "
"estiver ativada, as superposições serão removidas"

#: settings_fields.php:203
msgid "Adds tabs along the top of the book"
msgstr "Adiciona guias ao longo do topo do livro"

#: settings_fields.php:210
msgid "Tab Width"
msgstr "Largura da guia"

#: settings_fields.php:211
msgid "Set the width of each tab. Can be a number or CSS string value"
msgstr ""
"Defina a largura de cada guia. Pode ser um número ou valor de seqüência de "
"caracteres CSS"

#: settings_fields.php:218
msgid "Tab Height"
msgstr "Altura da guia"

#: settings_fields.php:219
msgid "Set the height of each tab. Can be a number or CSS string value"
msgstr ""
"Defina a altura de cada guia. Pode ser um número ou valor de seqüência de "
"caracteres CSS"

#: settings_fields.php:226
msgid "Next Text"
msgstr "Próximo texto"

#: settings_fields.php:227
msgid "Set the inline text for all \"next\" controls (tabs, arrows, etc.)"
msgstr ""
"Defina o texto incorporado para todos os \"próximos\" controles (guias, "
"setas, etc.)"

#: settings_fields.php:234
msgid "Previous Text"
msgstr "Texto anterior"

#: settings_fields.php:235
msgid "Set the inline text for all \"previous\" controls (tabs, arrows, etc.)"
msgstr ""
"Defina o texto incorporado para todos os controles \"anteriores\" (guias, "
"setas, etc.)"

#: settings_fields.php:242
msgid "Next Title"
msgstr "Próximo Título"

#: settings_fields.php:243
msgid ""
"Set the text for the title attributes of all \"next\" controls (tabs, "
"arrows, etc.)"
msgstr ""
"Defina o texto para os atributos de título de todos os \"próximos\" "
"controles (guias, setas, etc.)"

#: settings_fields.php:250
msgid "Previous Title"
msgstr "Título anterior"

#: settings_fields.php:251
msgid ""
"Set text for title attributes of all \"previous\" controls (tabs, arrows, "
"etc.)"
msgstr ""
"Definir texto para atributos de título de todos os controles \"anteriores"
"\" (guias, setas, etc.)"

#: settings_fields.php:259
msgid "Check to display arrows for navigation"
msgstr "Verificar para exibir as setas para navegação"

#: settings_fields.php:266
msgid "Auto Hide Arrows"
msgstr "Auto ocultar as setas"

#: settings_fields.php:267
msgid "Auto hide the arrows when the controls are not hovered"
msgstr ""
"Ocultar automaticamente as setas quando os controles não estão pairados"

#: settings_fields.php:274
msgid "Cursor"
msgstr "Cursor"

#: settings_fields.php:275
msgid ""
"The CSS cursor used for all controls (tabs, arrows, etc.). Accepts any CSS "
"Cursor"
msgstr ""
"O cursor CSS usado para todos os controles (guias, setas, etc.). Aceita "
"qualquer Cursor CSS"

#: settings_fields.php:282
msgid "Hash"
msgstr "Jogo da velha"

#: settings_fields.php:283
msgid "Enables navigation using a hash string (e.g. \"#/page/1\")"
msgstr ""
"Habilita a navegação usando uma seqüência de hash (por exemplo, \"# / page / "
"1\")"

#: settings_fields.php:290
msgid "Hash Title Text"
msgstr "Texto do Título de Hash"

#: settings_fields.php:291
msgid "Text which forms the hash page title (e.g. \"Book - Page 1\")"
msgstr ""
"Texto que forma o título da página hash (por exemplo, \"Livro - Página 1\")"

#: settings_fields.php:299
msgid "Check to enable page flipping by keyboard arrow keys"
msgstr "Marque para habilitar a página movendo com as setas do teclado"

#: settings_fields.php:306 settings_fields.php:312
msgid "Auto Play"
msgstr "Reprodução automática"

#: settings_fields.php:313
msgid "Check to enable auto flipping pages"
msgstr "Marque para ativar as páginas de deslocamento automático"

#: settings_fields.php:320
msgid "Auto Play Delay"
msgstr "Retardo de Reprodução Automática"

#: settings_fields.php:321
msgid "The time in milliseconds between each automatic page flip transition"
msgstr ""
"O tempo, em milissegundos, entre cada transição automática do flip de página"

#: settings_fields.php:328 settings_fields.php:334
msgid "Zoom"
msgstr "Zoom"

#: settings_fields.php:335
msgid "Enable Zoom on hover"
msgstr "Ativar zoom em foco"

#: settings_fields.php:342
msgid "Zoom Depth"
msgstr "Ativar zoom em foco"

#: settings_fields.php:343
msgid "Provide zoom depth, 1 for 100%, 2 for 200% etc"
msgstr "Fornecer profundidade de zoom, 1 para 100%, 2 para 200% etc"

#: settings_fields.php:351 settings_fields.php:357
msgid "Shadows"
msgstr "Sombras"

#: settings_fields.php:358
msgid "Display shadows on pages during animations"
msgstr "Exibir sombras nas páginas durante as animações"

#: settings_fields.php:366
msgid "Shadow Top Forward Width"
msgstr "Shadow Largura dianteira superior"

#: settings_fields.php:367
msgid ""
"The width of the top forward shadow. Only change if you change the shadow "
"images"
msgstr ""
"A largura da sombra de frente superior. Alterar apenas se você alterar as "
"imagens de sombra"

#: settings_fields.php:375
msgid "Shadow Top Back Width"
msgstr "Sombra Largura superior traseira"

#: settings_fields.php:376
msgid ""
"The width of the top back animation shadow. Only change if you change the "
"shadow images"
msgstr ""
"A largura da sombra de animação traseira superior. Alterar apenas se você "
"alterar as imagens de sombra"

#: settings_fields.php:384
msgid "Shadow Bottom Width"
msgstr "Largura inferior da sombra"

#: settings_fields.php:385
msgid ""
"The width of the bottom animation shadow. Only change if you change the "
"shadow images"
msgstr ""
"A largura da sombra de animação inferior. Alterar apenas se você alterar as "
"imagens de sombra"

#: settings_fields.php:393
msgid "Toolbar"
msgstr "Barra de ferramentas"

#: settings_fields.php:399
msgid "Bottom Toolbar"
msgstr "Barra de ferramentas inferior"

#: settings_fields.php:400
msgid "It will display bottom toolbar for navigation"
msgstr "Ele exibirá a barra de ferramentas inferior para navegação"

#: settings_fields.php:408
msgid "Toolbar Background Color"
msgstr "Cor de fundo da barra de ferramentas"

#: settings_fields.php:409
msgid "Choose toolbar background color"
msgstr "Escolha a cor de fundo da barra de ferramentas"

#: settings_fields.php:417
msgid "Toolbar Text Color"
msgstr "Cor do texto da barra de ferramentas"

#: settings_fields.php:418
msgid "Choose toolbar text color"
msgstr "Escolha a cor de texto da barra de ferramentas"

#: settings_fields.php:426
msgid "Enable Popup"
msgstr "Ativar pop-up"

#: settings_fields.php:427
msgid "A popup button will appear to open pages in popup"
msgstr "Um botão pop-up aparecerá para abrir páginas em pop-up"

#: settings_fields.php:435
msgid "Book Front Title"
msgstr "Título da frente do livro"

#: settings_fields.php:436
msgid "Determines title of blank starting page"
msgstr "Determina o título da página inicial em branco"

#: settings_fields.php:444
msgid "Book Front Chapter"
msgstr "Capítulo da frente do livro"

#: settings_fields.php:445
msgid "Determines chapter name of blank starting page"
msgstr "Determina o nome do capítulo da página inicial em branco"

#: settings_fields.php:453
msgid "Book Back Title"
msgstr "Livro Voltar Título"

#: settings_fields.php:454
msgid "Determines title of blank ending page"
msgstr "Determina o título da página final em branco"

#: settings_fields.php:462
msgid "Book Back Chapter"
msgstr "Livro Voltar Capítulo"

#: settings_fields.php:463
msgid "Determines chapter name of blank ending page"
msgstr "Determina o nome do capítulo da página final em branco"

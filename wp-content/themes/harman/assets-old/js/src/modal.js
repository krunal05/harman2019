jQuery(document).ready(function($) {

    // Show form modal
    $(".modal-trigger").on("click", showModal);

    // Trigger the modal close and overlay
    $(".modal-close").on("click", closeModal);
    $("#modal-overlay").on("click", closeModal);

    /**
     * Shows the modal
     */
    function showModal() {

        var target = $(this).attr("data-target");
        var holderClass = $(this).attr("data-holder");
        var content = document.getElementById(target);

        $("#modal").find(".modal-content").html(content);

        // Show
        $("#modal").fadeIn();
        $("#modal").addClass("fade in");
        $("#modal").addClass(holderClass);
        $("#modal-overlay").fadeIn().addClass("active");

        // Add a target to close
        $("#modal").find(".modal-close").attr("data-target", target);

    };

    /**
     * Closes the modal
     * @returns {boolean}
     */
    function closeModal() {

        $("#modal").removeClass("fade in");
        $("#modal").fadeOut();
        $("#modal-overlay").fadeOut().removeClass("active");

        // Get the close button
        var button = $(this).hasClass("modal-close") ? $(this) : $(this).closest("#modal").find(".modal-close");

        // Get the target
        var target = button.attr("data-target");
        var modalContent = button.closest("#modal").find(".modal-content");

        // Add the content back
        $("."+target).html(modalContent.html());

        setTimeout(function(){
            $("#modal").removeClass();
        }, 300);

        return false;
    }
});
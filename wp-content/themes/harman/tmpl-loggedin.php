<?php
/**
 * Template Name: Logged In Full Width
 */

get_header(); ?>

    <div id="page-wrap">
		<div id="primary" class="content-area">
			<main id="main" class="site-main" role="main">

				<?php
				/* Start the Loop */
				while ( have_posts() ) : the_post();
					if( is_user_logged_in() ) {
						get_template_part( 'template-parts/page/content', 'page' );
					} else {
						get_template_part( 'template-parts/page/content', 'locked' );
					}
				endwhile; // End of the loop.
				?>

			</main><!-- #main -->
		</div><!-- #primary -->
	</div>

<?php get_footer();

<?php
/**
 * Button Shortcodes
 * @param $atts
 * @return string
 */
function gp_button( $atts ) {

	$button = shortcode_atts(array(
		'size'    => 'large',
		'link'    => '#',
		'text'    => 'Learn More',
		'title'   => 'Learn More',
		'color'   => 'white',
		'animate' => ''
	), $atts);

	// Class
	$class = "button " . $button['size'];

	// Add color
	$class .= ' ' . $button['color'];

	// Check for animation
	if( empty( $button['animate'] ) === false ) {
		$class .= ' wow ' . $button['animate'];
	}

	$html = '<a href="'. $button['link'] .'" title="'. $button['title'] .'" class="'. $class .'">'. $button['text'] .'</a>';

	return $html;

}
add_shortcode( 'button', 'gp_button' );

/**
 * Renders the about labels shortcode
 * @param $atts
 * @return string
 */
function harman_show_labels( $atts ) {

	// Atts
	$atts = shortcode_atts( array(
		'title'   => 'Our Labels',
	), $atts);

	ob_start();

	// Get labels
	$labels = gp_get_posts( array('post_type' => 'about-labels', 'posts_per_page' => 15, 'orderby' => 'date', 'order' => 'DESC') );
	if( $labels->have_posts() ) {

		?>
		<div class="our-labels-panel">
			<h2 class="label-title"><?php echo esc_html( $atts['title'] ); ?></h2>
			<ul class="labels">
				<?php while( $labels->have_posts() ) :
					$labels->the_post();
					?>
					<li>
						<?php echo get_the_post_thumbnail(null, 'full'); ?>
					</li>
					<?php
				endwhile;
				?>
			</ul>
		</div>
		<?php
	}
	wp_reset_postdata();

	// End buffer
	$html = ob_get_clean();

	// Return
	return $html;

}
add_shortcode( 'show_about_labels', 'harman_show_labels' );

/**
 * Renders the registration form shortcode
 * @param $atts
 * @return string
 */
function harman_registration_form( $atts ) {
	$atts = shortcode_atts( array(
		'title' => 'Register'
	), $atts, 'harman_registration_form' );

	ob_start();
    ?>
    <div id="register-form">
        <form method="POST" class="form ajax" action="<?php echo get_admin_url(null, 'admin-ajax.php'); ?>" enctype="multipart/form-data">

            <div class="form-columns">
                <div class="column">
                    <h2 class="column-heading">Contact Information</h2>

                    <div class="form-row">
                        <div class="form-field">
                            <label for="firstname">First Name: <span class="required-tip">*</span></label>
                            <div class="field-wrapper">
                                <input type="text" name="firstname" id="firstname" class="input input-text required" value=""/>
                                <div class="tooltip"></div>
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-field">
                            <label for="lastname">Last Name: <span class="required-tip">*</span></label>
                            <div class="field-wrapper">
                                <input type="text" name="lastname" id="lastname" class="input input-text required" value=""/>
                                <div class="tooltip"></div>
                            </div>
                        </div>
                    </div>

                    <div class="form-row left">
                        <div class="form-field">
                            <label for="email">Email <span class="required-tip">*</span></label>
                            <div class="field-wrapper">
                                <input type="email" name="email" id="email" class="input input-email required" value="" />
                                <div class="tooltip"></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-row right">
                        <div class="form-field">
                            <label for="phone">Phone <span class="required-tip">*</span></label>
                            <div class="field-wrapper">
                                <input type="phone" name="phone" id="phone" class="input input-phone required" value="" />
                                <div class="tooltip"></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-row right">
                        <div class="form-field">
                            <label for="fax">Fax </label>
                            <div class="field-wrapper">
                                <input type="text" name="fax" id="fax" class="input input-fax" value="" />
                                <div class="tooltip"></div>
                            </div>
                        </div>
                    </div>

                    <h2 class="divider-heading">Billing Address</h2>
                    <div class="form-row">
                        <div class="form-field">
                            <label for="address_1">Address line 1: <span class="required-tip">*</span></label>
                            <div class="field-wrapper">
                                <input type="text" name="address1" id="address_1" class="input input-text required" value=""/>
                                <div class="tooltip"></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-field">
                            <label for="address_2">Address line 2: </label>
                            <div class="field-wrapper">
                                <input type="text" name="address2" id="address_2" class="input input-text" value=""/>
                                <div class="tooltip"></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-field">
                            <label for="city">City: <span class="required-tip">*</span></label>
                            <div class="field-wrapper">
                                <input type="text" name="city" id="city" class="input input-text required" value=""/>
                                <div class="tooltip"></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-field">
                            <label for="province">State/Province: <span class="required-tip">*</span></label>
                            <div class="field-wrapper">
                                <input type="text" name="province" id="province" class="input input-text required" value=""/>
                                <div class="tooltip"></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-field">
                            <label for="country">Country: <span class="required-tip">*</span></label>
                            <div class="field-wrapper">
                                <input type="text" name="country" id="country" class="input input-text required" value=""/>
                                <div class="tooltip"></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-field">
                            <label for="postal_code">Zip/Postal Code: <span class="required-tip">*</span></label>
                            <div class="field-wrapper">
                                <input type="text" name="postalcode" id="postal_code" class="input input-text required" value=""/>
                                <div class="tooltip"></div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="column">

                    <h2 class="column-heading">Company Information</h2>
                    <div class="form-row">
                        <div class="form-field">
                            <label for="company_name">Company Name: <span class="required-tip">*</span></label>
                            <div class="field-wrapper">
                                <input type="text" name="companyname" id="company_name" class="input input-text required" value=""/>
                                <div class="tooltip"></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-field">
                            <label for="sales_tax_id">Sales Tax ID: <span class="required-tip">*</span></label>
                            <div class="field-wrapper">
                                <input type="text" name="salestaxid" id="sales_tax_id" class="input input-text required" value=""/>
                                <div class="tooltip"></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-field">
                            <label for="tax_province">Sales Tax State/Province: <span class="required-tip">*</span></label>
                            <div class="field-wrapper">
                                <input type="text" name="taxprovince" id="tax_province" class="input input-text required" value=""/>
                                <div class="tooltip"></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-field">
                            <label for="federal_id">Federal ID #: </label>
                            <div class="field-wrapper">
                                <input type="text" name="federalid" id="federal_id" class="input input-text" value=""/>
                                <div class="tooltip"></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-field">
                            <label for="website">Company Website or Social Media: </label>
                            <div class="field-wrapper">
                                <input type="text" name="website" id="website" class="input input-text input-url" value=""/>
                                <div class="tooltip"></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-field">
                            <label for="store_type">Type of Store: <span class="required-tip">*</span></label>
                            <div class="field-wrapper">
                                <select name="storetype" id="store_type" class="input select required">
                                    <option value="">Select</option>
                                    <option value="Retailer">Retailer</option><option value="Distributor">Distributor</option><option value="Manufacturer">Manufacturer</option><option value="Sales Rep">Sales Rep</option><option value="Wholesaler">Wholesaler</option>
                                </select>
                                <div class="tooltip"></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-field">
                            <label for="store_category">Store Category:</label>
                            <div class="field-wrapper">
                                <select name="storecategory" id="store_category" class="input select">
                                    <option value="">Select</option>
                                    <option value="Art Gallery">Art Gallery</option><option value="Beauty Salon/Spa">Beauty Salon/Spa</option><option value="Boutique/Apparel">Boutique/Apparel</option><option value="Card/Book Store">Card/Book Store</option><option value="Caterer/Restaurant/Hotel">Caterer/Restaurant/Hotel</option><option value="Christmas Store">Christmas Store</option><option value="Convenience/Grocery">Convenience/Grocery</option><option value="Corporate Buyer">Corporate Buyer</option><option value="Craft/Hobby/Scrapbooking">Craft/Hobby/Scrapbooking</option><option value="Department Store">Department Store</option><option value="Florist">Florist</option><option value="Furniture/Lighting">Furniture/Lighting</option><option value="Garden">Garden</option><option value="Gift Shop">Gift Shop</option><option value="Gourmet Food/Wine">Gourmet Food/Wine</option><option value="Hardware/Home Improvement">Hardware/Home Improvement</option><option value="Home Decor/Accessories">Home Decor/Accessories</option><option value="Interior Design">Interior Design</option><option value="Jewelry Store">Jewelry Store</option><option value="Mail Order Catalog">Mail Order Catalog</option><option value="Mass Market">Mass Market</option><option value="Pet/Vet">Pet/Vet</option><option value="Pharmacy/Medical">Pharmacy/Medical</option><option value="Resort/Theme Park">Resort/Theme Park</option><option value="School">School</option><option value="Souvenir">Souvenir</option><option value="Sports">Sports</option><option value="Tabletop">Tabletop</option><option value="Web/Online">Web/Online</option><option value="Other">Other</option>
                                </select>
                                <div class="tooltip"></div>
                            </div>
                        </div>
                    </div>

                    <h2 class="divider-heading">Login Information</h2>
                    <p class="text">
                        We recommend that you use your email address for your username, since common login names will already be taken.
                    </p>
                    <div class="form-row">
                        <div class="form-field">
                            <label for="username">Username: <span class="required-tip">*</span></label>
                            <div class="field-wrapper">
                                <input type="text" name="username" id="username" class="input input-text required" value=""/>
                                <div class="tooltip"></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-field">
                            <label for="password">Password: <span class="required-tip">*</span></label>
                            <div class="field-wrapper">
                                <input type="password" name="password" id="password" class="input input-text required" value=""/>
                                <div class="tooltip"></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-field">
                            <label for="password-confirm">Confirm Password: <span class="required-tip">*</span></label>
                            <div class="field-wrapper">
                                <input type="password" name="passwordconfirm" id="password-confirm" class="input input-text required" value=""/>
                                <div class="tooltip"></div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <h2 class="column-heading">Marketing Information</h2>

            <div class="form-field-wrapper">
                <div class="form-fields first">
                    <div class="form-row">
                        <div class="form-field">
                            <label for="has_sales_rep">Sales Rep?</label>
                            <div id="sales-rep-radio" class="radio-wrap">
                                <div class="field-wrapper">
                                    <input type="radio" name="has_sales_rep" id="has-rep-yes" class="" value="Yes" />
                                    <label for="has-rep-yes">Yes</label>
                                </div>
                                <div class="field-wrapper">
                                    <input type="radio" name="has_sales_rep" id="has-rep-no" class="" value="No" />
                                    <label for="has-rep-no">No</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-field">
                            <label for="sales_rep_name">Sales Rep Name:</label>
                            <div class="field-wrapper">
                                <input type="text" name="salesrepname" id="sales_rep_name" class="input input-text" value=""/>
                                <div class="tooltip"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-fields last">
                    <div class="form-row">
                        <div class="form-field">
                            <label for="no_of_stores">How many stores?</label>
                            <div class="field-wrapper">
                                <select name="noofstores" id="no_of_stores" class="input select">
                                    <option value="">Select</option>
                                    <option value="1">1</option>
                                    <option value="2-4">2-4</option>
                                    <option value="5-10">5-10</option>
                                    <option value="11-24">11-24</option>
                                    <option value="25+">25+</option>
                                </select>
                                <div class="tooltip"></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-field">
                            <label for="hear_about_us">How did you hear about us?</label>
                            <div class="field-wrapper">
                                <select name="hearaboutus" id="hear_about_us" class="input select">
                                    <option value="">Select</option>
                                    <option value="Magazine">Magazine</option>
                                    <option value="Email Ad">Email Ad</option>
                                    <option value="Trade Show">Trade Show</option>
                                    <option value="Gift Mart">Gift Mart</option>
                                    <option value="Web Search">Web Search</option>
                                    <option value="Referral">Referral</option>
                                    <option value="Other">Other</option>
                                </select>
                                <div class="tooltip"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-row">
                <div class="form-field">
                    <label for="join_email_list">Join email list?</label>
                    <div class="field-wrapper">
                        <select name="joinemaillist" id="join_email_list" class="input select">
                            <option value="Yes">Yes</option>
                            <option value="No">No</option>
                        </select>
                        <div class="tooltip"></div>
                    </div>
                </div>
            </div>

            <div class="form-row">
                <div class="form-response"></div>
            </div>

            <div class="form-row">
                <div class="form-field submit-field">
			        <?php wp_nonce_field('n-registration', 'n_registration'); ?>
                    <input type="hidden" name="action" value="send_registration" />
                    <button type="submit" name="submit" class="button form-button">
                        <span class="button-text">Register</span>
                    </button>
                </div>
            </div>

        </form>
    </div>
    <?php
	$html = ob_get_clean();
	return $html;
}
add_shortcode('harman_registration_form', 'harman_registration_form');

/**
 * Handles ajax registration request
 */
function harman_ajax_send_registration() {

	// Check ajax referrer
	check_ajax_referer( 'n-registration', 'n_registration' );

	// Default field for marking errors
	$field = '';

	try{

		// Data
		$first_name = isset($_POST['firstname']) ? wc_clean($_POST['firstname']) : '';
		$last_name = isset($_POST['lastname']) ? wc_clean($_POST['lastname']) : '';
		$email = isset($_POST['email']) ? wc_clean($_POST['email']) : '';
		$phone = isset($_POST['phone']) ? wc_clean($_POST['phone']) : '';
		$address_1 = isset($_POST['address1']) ? wc_clean($_POST['address1']) : '';
		$city = isset($_POST['city']) ? wc_clean($_POST['city']) : '';
		$province = isset($_POST['province']) ? wc_clean($_POST['province']) : '';
		$country = isset($_POST['country']) ? wc_clean($_POST['country']) : '';
		$postal_code = isset($_POST['postalcode']) ? wc_clean($_POST['postalcode']) : '';
		$company_name = isset($_POST['companyname']) ? wc_clean($_POST['companyname']) : '';
		$sales_tax_id = isset($_POST['salestaxid']) ? wc_clean($_POST['salestaxid']) : '';
		$tax_province = isset($_POST['taxprovince']) ? wc_clean($_POST['taxprovince']) : '';
		$store_type = isset($_POST['storetype']) ? wc_clean($_POST['storetype']) : '';
		$username = isset($_POST['username']) ? wc_clean($_POST['username']) : '';
		$password = isset($_POST['password']) ? wc_clean($_POST['password']) : '';
		$password_confirm = isset($_POST['passwordconfirm']) ? wc_clean($_POST['passwordconfirm']) : '';

		$address_2 = isset($_POST['address2']) ? wc_clean($_POST['address2']) : '';
		$fax = isset($_POST['fax']) ? wc_clean($_POST['fax']) : '';
		$federal_id = isset($_POST['federalid']) ? wc_clean($_POST['federalid']) : '';
		$website = isset($_POST['website']) ? wc_clean($_POST['website']) : '';
		$store_category = isset($_POST['storecategory']) ? wc_clean($_POST['storecategory']) : '';
		$has_sales_rep = isset($_POST['has_sales_rep']) ? wc_clean($_POST['has_sales_rep']) : '';
		$sales_rep_name = isset($_POST['salesrepname']) ? wc_clean($_POST['salesrepname']) : '';
		$no_of_stores = isset($_POST['noofstores']) ? wc_clean($_POST['noofstores']) : '';
		$hear_about_us = isset($_POST['hearaboutus']) ? wc_clean($_POST['hearaboutus']) : '';
		$join_email_list = isset($_POST['joinemaillist']) ? wc_clean($_POST['joinemaillist']) : '';

		// Make sure the required data is entered
		if( empty($first_name) || empty($last_name) || empty($address_1) || empty($city) || empty($province) || empty($country) || empty($postal_code) || empty($phone) || empty($company_name) || empty($sales_tax_id) || empty($tax_province) || empty($store_type) || empty($username) || empty($email) ) {
			throw new Exception('Please correct all the errors');
		}

		// Email needs to be valid
		if( empty($email) || !filter_var($email, FILTER_VALIDATE_EMAIL) ) {
			$field = '#email';
			throw new Exception('Please provide a valid email');
		}

		// Passwords
		if( empty($password) || empty( $password_confirm ) ) {
			throw new Exception('Please enter a valid password');
		}

		// Password length
		if( strlen( $password ) < 8 ) {
			throw new Exception( 'Password should be at least 8 characters' );
		}

		// Check if passwords match
		if ( $password != $password_confirm ) {
			throw new Exception('The passwords do not match');
		}

		// Make sure username doesn't exist already
		global $wpdb;
		$user = $wpdb->get_row( $wpdb->prepare(
			"SELECT * FROM $wpdb->users WHERE `user_nicename` = %s", $username
		) );

		if( $user ) {
		    $field = '#username';
			throw new Exception( 'This username is unavailable' );
		}

		$fullname = sprintf('%s %s', $first_name, $last_name);

		add_filter( 'wp_mail_content_type', 'harman_set_html_mail_content_type' );
		$to = get_option('admin_email');
		$subject = "New Registration Form Submission from ". $fullname ." -  Harman";

		$body = "<h2>Contact Details</h2>";
		$body .= "<table>";
		$body .= "<tr>";
		$body .= "<td><b>First Name:</b> </td> <td>". $first_name ."</td>";
		$body .= "</tr>";
		$body .= "<tr>";
		$body .= "<td><b>Last Name:</b> </td> <td>". $last_name ."</td>";
		$body .= "</tr>";
		$body .= "<tr>";
		$body .= "<td><b>Email:</b> </td> <td>". $email ."</td>";
		$body .= "</tr>";
		$body .= "<tr>";
		$body .= "<td><b>Phone:</b> </td> <td>". $phone ."</td>";
		$body .= "</tr>";
		$body .= "<tr>";
		$body .= "<td><b>Fax:</b> </td> <td>". $fax ."</td>";
		$body .= "</tr>";
		$body .= "</table>";

		$body .= "<h2>Billing Address</h2>";
		$body .= "<table>";
		$body .= "<tr>";
		$body .= "<td><b>Address Line 1:</b> </td> <td>". $address_1 ."</td>";
		$body .= "</tr>";
		$body .= "<tr>";
		$body .= "<td><b>Address Line 2:</b> </td> <td>". $address_2 ."</td>";
		$body .= "</tr>";
		$body .= "<tr>";
		$body .= "<td><b>City:</b> </td> <td>". $city ."</td>";
		$body .= "</tr>";
		$body .= "<tr>";
		$body .= "<td><b>Province:</b> </td> <td>". $province ."</td>";
		$body .= "</tr>";
		$body .= "<tr>";
		$body .= "<td><b>Postal Code:</b> </td> <td>". $postal_code ."</td>";
		$body .= "</tr>";
		$body .= "</table>";

		$body .= "<h2>Company Information</h2>";
		$body .= "<table>";
		$body .= "<tr>";
		$body .= "<td><b>Company Name:</b> </td> <td>". $company_name ."</td>";
		$body .= "</tr>";
		$body .= "<tr>";
		$body .= "<td><b>Sales Tax ID: </b></td> <td>". $sales_tax_id ."</td>";
		$body .= "</tr>";
		$body .= "<tr>";
		$body .= "<td><b>Sales Tax State/Province:</b> </td> <td>". $tax_province ."</td>";
		$body .= "</tr>";
		$body .= "<tr>";
		$body .= "<td><b>Federal ID #:</b> </td> <td>". $federal_id ."</td>";
		$body .= "</tr>";
		$body .= "<tr>";
		$body .= "<td><b>Company Website or Social Media:</b> </td> <td>". $website ."</td>";
		$body .= "</tr>";
		$body .= "<tr>";
		$body .= "<td><b>Type of Store:</b> </td> <td>". $store_type ."</td>";
		$body .= "</tr>";
		$body .= "<tr>";
		$body .= "<td><b>Store Category:</b> </td> <td>". $store_category ."</td>";
		$body .= "</tr>";
		$body .= "</table>";

		$body .= "<h2>Login Information</h2>";
		$body .= "<table>";
		$body .= "<tr>";
		$body .= "<td><b>Username:</b> </td> <td>". $username ."</td>";
		$body .= "</tr>";
		$body .= "<tr>";
		$body .= "<td><b>Password: </b></td> <td>". $password ."</td>";
		$body .= "</tr>";
		$body .= "</table>";

		$body .= "<h2>Marketing Information</h2>";
		$body .= "<table>";
		$body .= "<tr>";
		$body .= "<td><b>Has Sales Rep?:</b> </td> <td>". $has_sales_rep ."</td>";
		$body .= "</tr>";
		$body .= "<tr>";
		$body .= "<td><b>Sales Rep Name:</b> </td> <td>". $sales_rep_name ."</td>";
		$body .= "</tr>";
		$body .= "<tr>";
		$body .= "<td><b>How many stores?</b> </td> <td>". $no_of_stores ."</td>";
		$body .= "</tr>";
		$body .= "<tr>";
		$body .= "<td><b>How did you hear about us?</b> </td> <td>". $hear_about_us ."</td>";
		$body .= "</tr>";
		$body .= "<tr>";
		$body .= "<td><b>Join Email List?</b> </td> <td>". $join_email_list ."</td>";
		$body .= "</tr>";
		$body .= "</table>";

		// Send email
		$sent = wp_mail($to, $subject, $body);

		// Notify admin of an error in email
		if( !$sent ) {
		    // Retry
			wp_mail(get_option('admin_email'), 'Email failed: Registration Page', 'This is to notify you that the registration form email failed to send with following details:' . $body);
		}

		// Response
		$response = array(
			'message' => !$sent ? 'We are unable to send your request at this time. Please try again later.' : 'Thank you for registering with Harman. We will be in touch as soon as possible.',
			'error'   => $sent ? false : true,
			'field'   => $field,
		);

		remove_filter( 'wp_mail_content_type', 'harman_set_html_mail_content_type' );

	} catch (Exception $e) {
		$response = array(
			'message' => $e->getMessage(),
			'error'   => true,
			'field'   => $field,
		);
    }

	// Return
	echo json_encode($response);
	exit;

}

// Ajax handlers
add_action('wp_ajax_send_registration', 'harman_ajax_send_registration');
add_action('wp_ajax_nopriv_send_registration', 'harman_ajax_send_registration');

/**
 * Change email content type to html
 * @return string
 */
function harman_set_html_mail_content_type() {
	return 'text/html';
}

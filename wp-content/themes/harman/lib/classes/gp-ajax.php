<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}



class GP_Ajax{

    /**
     * GP_Ajax constructor.
     */
    public function __construct() {

        // Initialize all ajax requests

        add_action( 'wp_ajax_salesrep_keyword_search', array( $this, 'salesrep_keyword_search' ) );


        add_action( 'wp_ajax_salesrep_range_search', array( $this, 'salesrep_range_search' ) );

    }



    /*****************************************************
     * Sales Rep - Keyword Search
     ****************************************************/
    function salesrep_keyword_search()
    {
        check_ajax_referer('security-keyword-search-nonce', 'security');
        global $wpdb;

        $params = $_POST['data'];
        parse_str($params, $data);
        $keyword = sanitize_text_field($data['keyword']);

        

        $query = "
                            SELECT $wpdb->posts.ID , $wpdb->posts.post_title
                            FROM $wpdb->posts
                            WHERE (
                            $wpdb->posts.post_title LIKE '%$keyword%' || 
                            $wpdb->posts.post_content LIKE '%$keyword%' ||  
                            (SELECT $wpdb->postmeta.post_id from $wpdb->postmeta where $wpdb->postmeta.post_id = $wpdb->posts.ID AND $wpdb->postmeta.meta_key = '_sku' AND $wpdb->postmeta.meta_value  LIKE '%$keyword%')
                            )
                            AND   $wpdb->posts.post_status = 'publish'     
                            AND TRIM($wpdb->posts.post_title) <> ''
                            AND ($wpdb->posts.post_type = 'product' || $wpdb->posts.post_type = 'product_variation')
                            GROUP BY $wpdb->posts.ID ORDER BY $wpdb->posts.ID ASC  LIMIT 200
                        ";
        $sql_result = $wpdb->get_results( $query, OBJECT);
        $total = count($sql_result);

        if($total > 0){
        ?>

            <ul class="products columns-3">
                <?php  foreach ($sql_result as $product) {
                    $product_id = $product->ID;
                    $product_title = $product->post_title;
                    $product_link = get_the_permalink($product_id);
                    $prod_image = get_product_image($product_id,'medium');
                    $stock = get_post_meta( $product_id, '_stock', true );
                    $min_order =  get_post_meta( $product_id, 'min_order', true );
                    $sku = get_post_meta( $product_id, '_sku', true );

//                    $product = wc_get_product($product_id);
//                    if( $product->has_child() ) {
//                        // $product_type = "ID :$product_id , $product_title -> is variable";
//                        $variation_ids   = $product->get_children( $args = '', $output = OBJECT );
//                        if(count($variation_ids) > 0){
//                            $min_order =  get_post_meta( current($variation_ids), 'min_order', true );
//                        }else{
//                            $min_order ="";
//                        }
//                    }else{
//                        //  $product_type =  "ID :$product_id ,$product_title -> is simple";
//                        $min_order =  get_post_meta( $product_id, 'min_order', true );
//                    }
                    ?>
                    <li class="type-product">
                        <div class="single-loop-product">
                            <div class="prod-wrapper ">
                                <a href="<?php echo $product_link; ?>">
                                    <div class="prod-img"  style="background-image: url('<?php echo $prod_image; ?>')"></div>
                                </a>
                            </div>
                            <div class="prod-detail">
                                <a href="<?php echo $product_link; ?>">
                                    <h3 class="prod-title"><?php echo $product_title; ?></h3>
                                </a>
                                <?php  if($stock == "" || $stock == 0){ ?>  <a href="javascript:void(0);" class="contact-harman">Contact Harman for ETA</a>
                                <?php  } else { ?> <h4 >On Hand: <?php echo $stock; ?></h4> <?php  } ?>
                                <?php if($min_order != "") { ?><h6 >Minimum Order: <?php echo $min_order; ?></h6>  <?php } ?>
                                <h6>SKU : <?php echo $sku; ?></h6>
                            </div>
                        </div>
                    </li>
                <?php } ?>
            </ul>

        <?php
        }
        exit;
    }



    /*****************************************************
     * Sales Rep - Range Search
     ****************************************************/
    function salesrep_range_search()
    {
        check_ajax_referer('security-range-search-nonce', 'security');
        global $wpdb;

        $params = $_POST['data'];
        parse_str($params, $data);
        $start_range = sanitize_text_field($data['start-range-sku']);
        $end_range = sanitize_text_field($data['end-range-sku']);

        $convert_sku_to_digit = "  regexp_replace(cu.meta_value,'[[:alpha:]]','') ";

        $query = "
                            SELECT $wpdb->posts.ID , $wpdb->posts.post_title, $wpdb->posts.post_parent
                            FROM $wpdb->posts                            
                            INNER JOIN $wpdb->postmeta AS cu ON ($wpdb->posts.ID = cu.post_id AND cu.meta_key='_sku' AND $convert_sku_to_digit  >= $start_range AND $convert_sku_to_digit  <= $end_range)                            
                            AND   $wpdb->posts.post_status = 'publish'     
                            AND TRIM($wpdb->posts.post_title) <> ''
                            AND ($wpdb->posts.post_type = 'product' || $wpdb->posts.post_type = 'product_variation')
                            GROUP BY $wpdb->posts.ID ORDER BY $wpdb->posts.ID ASC  LIMIT 200
                        ";

        $sql_result = $wpdb->get_results( $query, OBJECT);
        $total = count($sql_result);

        if($total > 0){
            ?>

            <ul class="products columns-3">
                <?php  foreach ($sql_result as $product) {
                    $product_id = $product->ID;
                    $product_title = $product->post_title;
                    $product_link = get_the_permalink($product_id);
                    $prod_image = get_product_image($product_id,'medium');
                    $stock = get_post_meta( $product_id, '_stock', true );
                    $sku = get_post_meta( $product_id, '_sku', true );
                    $post_type = get_post_type($product_id);

                    $min_order =  get_post_meta( $product_id, 'min_order', true );
//                    $product = wc_get_product($product_id);
//                    if( $product->has_child() ) {
//                         $product_type = "ID :$product_id , $product_title -> is variable <br>";
//                        $variation_ids   = $product->get_children( $args = '', $output = OBJECT );
//                        if(count($variation_ids) > 0){
//                            $min_order =  get_post_meta( current($variation_ids), 'min_order', true );
//                        }else{
//                            $min_order ="";
//                        }
//                    }else{
//                          $product_type =  "ID :$product_id ,$product_title -> is simple <br>";
//                        $min_order =  get_post_meta( $product_id, 'min_order', true );
//                    }
                    ?>
                    <li class="type-product">
                        <div class="single-loop-product">
                            <div class="prod-wrapper ">
                                <a href="<?php echo $product_link; ?>">
                                    <div class="prod-img"  style="background-image: url('<?php echo $prod_image; ?>')"></div>
                                </a>
                            </div>
                            <div class="prod-detail">
                                <a href="<?php echo $product_link; ?>">
                                    <h3 class="prod-title"><?php echo $product_title; ?></h3>
                                </a>
                                <?php  if($stock == "" || $stock == 0){ ?>  <a href="javascript:void(0);" class="contact-harman">Contact Harman for ETA</a>
                                <?php  } else { ?> <h4 >On Hand: <?php echo $stock; ?></h4> <?php  } ?>
                                <?php if($min_order != "") { ?><h6 >Minimum Order: <?php echo $min_order; ?></h6>  <?php } ?>

                                <?php // echo $product_type; ?>
<!--                                <h6>Post Type : --><?php //echo $post_type; ?><!--</h6>-->
                                <h6>SKU : <?php echo $sku; ?></h6>
                            </div>
                        </div>
                    </li>
                <?php } ?>
            </ul>

            <?php
        }
        exit;
    }



}

// Initialize
global $gp_ajax;
$gp_ajax = new GP_Ajax();
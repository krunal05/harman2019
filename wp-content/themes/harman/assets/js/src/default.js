jQuery(document).ready(function($) {

    // Search open/close
    $("#search-open").on("click", function(ev) {
        ev.preventDefault();
        // Show the search-box
        $("#search-box").addClass("show");
        // Add to header
        $(".site-header").addClass("show-search");
    });
    $("#search-close").on("click", function(ev) {
        ev.preventDefault();
        // Hide the search-box
        $("#search-box").removeClass("show");
        $(".site-header").removeClass("show-search");
    });

    //Smooth Scroll on anchor
    $(function() {
        $('a[href*="#"]:not([href="#"])').click(function() {
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                if ( target.length > 0 && target.is(":visible") ) {
                    $('html, body').animate({
                        scrollTop: target.offset().top
                    }, 500);
                    return false;
                }
            }
        });
    });

    // Get the header height
    var headerHeight = $("header.site-header").height();

    // Sticky header
    $(window).on("scroll", function() {

        if ( $(this).scrollTop() > headerHeight ) {
            $("body").addClass("sticky");
        } else {
            $("body").removeClass("sticky");
        }

    });

    // Sticky header on load
    if( $(window).scrollTop() > headerHeight ) {
        $("body").addClass("sticky")
    }

    // Add Moby
    if( $("#device-nav-holder").length > 0 ) {
        // Target the menu to be mobile friendly
        new Moby({
            menuClass:"right-side",
            menu: $(".navigation-top"),
            mobyTrigger: $("#device-nav-holder"), // Button that will trigger the Moby menu to open
            subMenuOpenIcon:'<i class="fa fa-angle-down" aria-hidden="true"></i>',
            subMenuCloseIcon:'<i class="fa fa-angle-up" aria-hidden="true"></i>',
            insertAfter: '<h1>edfdf</h1>',
            breakpoint: 1023
        });
    }


    $(".product-icon, .product-menu" ).hover(function(){
        $(".product-menu").addClass("product-menu-show");
    }, function(){
        $(".product-menu").removeClass("product-menu-show");
    });


});




$(window).on("load", function (e) {
    var product_menus = $( ".product-hover-menu ul" ).html();
    $(".product-menu #menu-product-menu").html(product_menus);
});
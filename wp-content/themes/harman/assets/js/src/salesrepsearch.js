jQuery(document).ready(function ($) {




    /**
     * Sales Rep - Keyword Search
     */
    $("#keyword_search").on("click",salesrep_keyword_search);

    $('.keyword-search-form').find('.required').keypress(function(e){
        if ( e.which == 13 ) // Enter key = keycode 13
        {
            $("#keyword_search").trigger('click');
            return false;
        }
    });


    /**
     * Sales Rep - Keyword Search
     */
    $("#range_search").on("click",salesrep_range_search);

    $('.range-search-form').find('.required').keypress(function(e){
        if ( e.which == 13 ) // Enter key = keycode 13
        {
            $("#range_search").trigger('click');
            return false;
        }
    });



    function salesrep_keyword_search(event) {
        event.preventDefault();
        console.log("Event : Keyword Search");
        var error = 0;
        var msg = "";


        $('.keyword-search-wrapper .form .required').each(function(){
            var fieldValue = $(this).val();
            if(fieldValue == ""){
                error = 1;
                msg = "Please Enter the Keyword";
            }
        });


        if(error == 1){
            alert(msg);
        } else {
            $('.message-alert').html("");

            var security = $("#security-keyword-search").val();
            var data = $('.keyword-search-form').serialize();
            show_loading();
            $.ajax({
                url: website_urls.ajax_url,
                type: 'POST',
                // dataType: 'json',
                data : {
                    action : 'salesrep_keyword_search',
                    data : data,
                    security : security,
                },
                success: function(response){
                    hide_loading();
                    $(".salesrep-search-result-wrapper").html(response);
                },
                error: function (response) {
                    hide_loading();
                    console.log("AJAX Error : something went wrong");
                }
            });
        }

    }


    function salesrep_range_search(event) {
        event.preventDefault();
        console.log("Event : Range Search");
        var error = 0;
        var msg = "";


        $('.range-search-wrapper .form .required').each(function(){
            var fieldValue = $(this).val();
            if(fieldValue == ""){
                error = 1;
                msg = "Please Enter the Start SKU and End SKU";
            }
        });


        if(error == 1){
            alert(msg);
        } else {
            $('.message-alert').html("");

            var security = $("#security-range-search").val();
            var data = $('.range-search-form ').serialize();
            show_loading();
            $.ajax({
                url: website_urls.ajax_url,
                type: 'POST',
                // dataType: 'json',
                data : {
                    action : 'salesrep_range_search',
                    data : data,
                    security : security,
                },
                success: function(response){
                    hide_loading();
                    console.log(response);
                    $(".salesrep-search-result-wrapper").html(response);
                },
                error: function (response) {
                    hide_loading();
                    console.log("AJAX Error : something went wrong");
                }
            });
        }

    }








    function show_loading(){
        $("body").css("opacity",0.6);
        $(".loading-image").removeClass("hidden");
    }


    function hide_loading(){
        $("body").css("opacity",1);
        $(".loading-image").addClass("hidden");
    }

});
jQuery(document).ready(function($) {

    // Check for form submission
    $("body").on("click", ".form-button", function() {

        // Check if the button is active
        if( $(this).hasClass('active') ){
            // Prevent the default
            event.preventDefault();
            return false;
        }

        // Get the form
        var form = $(this).closest('.form');

        // Remove any error class
        form.removeClass('error');

        // Set the button as active
        $(this).addClass('active');

        // Save the an reference to the button
        var button = this;

        // Passed validation or not
        var passed = true;

        // Get response field
        var responseField = form.find(".form-response");

        // Get the fields in the form
        var fields = form.find('.input');
        var i = 0;

        for( var length = fields.length; i < length; i++ ){

            // Get the parent of the input
            var parent = $(fields[i]).closest("div.form-field");
            try{

                // Validate the input
                validateInput.call(fields[i]);

                // Validate passed
                parent.removeClass('error');
                parent.addClass('valid');

            } catch( e ){

                // Set the error
                parent.find('.tooltip').text(e);
                parent.removeClass('valid');
                parent.addClass('error');

                // Failed validation
                passed = false;

            }
        }

        // Check if the validation passed
        if( passed === false ){
            // Validation failed
            // Caught an error
            $(button).removeClass('active');

            // Notify user about the error
            responseField.text( 'Please correct all the errors' );
            responseField.addClass("error").slideDown();

            // Prevent the default
            event.preventDefault();
            return false;
        }

        // Check if the form is ajaxed
        if( form.hasClass('ajax') ) {

            // Hide all tooltips errors
            form.find("div.form-field").removeClass("error");

            // Empty the response field, and remove the classes
            responseField.empty();
            responseField.removeClass('success error').slideUp();

            var action = form.attr("action");

            // Create the form
            var form_data = new FormData( form[0] );

            // Send the ajax request
            $.ajax({
                url      : action,
                data     : form_data,
                type     : 'POST',
                dataType : 'json',
                processData : false,
                contentType : false,
                success  : function(response){

                    // Check the response
                    if( response.error ) {

                        // Add error message
                        responseField.text( response.message );
                        responseField.removeClass("success").addClass("error").slideDown();

                        // Do we have an error field?, focus if yes
                        var errorField = response.field;
                        if(errorField.length > 0 ) {

                            // Get the parent
                            var fieldParent = form.find(errorField).closest("div.form-field");
                            fieldParent.removeClass("valid").addClass("error");

                            // Update the message
                            fieldParent.find(".tooltip").text( response.message );

                            // Focus on field
                            $(errorField).focus();

                            // Scroll to tooltip if not an input
                            if( !$(errorField).hasClass("input") ) {
                                $('html, body').animate({
                                    scrollTop: fieldParent.offset().top
                                }, 1000);
                            }
                        }

                    } else {

                        // Add success message
                        responseField.text( response.message );
                        responseField.removeClass("error").addClass("success").slideDown();

                        // Reset the form
                        form.trigger('reset');

                        // Scroll to message
                        $('html, body').animate({
                            scrollTop: responseField.offset().top
                        }, 1000);
                    }

                    // Remove the active class from the button
                    $(button).removeClass('active');

                }.bind(this),
                error: function() {
                    // Show a notification
                    responseField.text( 'There was an error sending the request' );
                    responseField.addClass("error").slideDown();
                    // Remove the active class
                    $(button).removeClass('active');
                }
            });

            // Prevent the default
            event.preventDefault();
            return false;

        } else {
            $(button).removeClass('active');
        }

    });


    /**
     * Input validation
     * Performs validation on an input field
     * this must be binded to the element
     */
    function validateInput(){

        // Get the value
        var value = $(this).val();

        // Check if the field is required
        if( $(this).hasClass('required') && value.length == 0 ){
            // No value entered on a required field
            throw 'Field is required';
        }

    }

});
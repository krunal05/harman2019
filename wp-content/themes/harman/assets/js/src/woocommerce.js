jQuery(document).ready(function() {

    $( ".variations_form" ).on( "found_variation", function(e, variation) {
        var $singleVariationWrap = $(".variations_form").find( '.single_variation_wrap' );
        var $qty           = $singleVariationWrap.find( '.quantity' );

        if( variation.step ) {
            $qty.find( 'input.qty' ).attr( 'step', variation.step );
        }

        // alert(variation.available_stock);
        if( variation.available_stock || variation.available_stock == "0" ) {
            $qty.find( 'input.qty' ).attr( 'available_stock', variation.available_stock );
        }
        console.log(variation);

    } );

    // Update input box after variation selects are blurred
    $('.variations select').bind('blur',function() {

        var start_min = $('.qty').attr('min');
        // Make sure input value is in bounds
        if ( start_min != '' ) {
            $('.qty').val(start_min);
        }

    });

    /*
     *	Make sure minimum equals value
     *	To Fix: when min = 0 and val = 1
     */
    if ( $("body.single-product .qty").val() != $("body.single-product .qty").attr('min') && $("body.single-product .qty").attr('min') != '' ) {
        $("body.single-product .qty").val( $("body.single-product .qty").attr('min') );
    }







    /**
     * Qty field focusout
     */
    $("body").on("focusout", "input.qty", function(){
        var qty = $(this).val();
        var step = $(this).attr("step");
        if( qty.length > 0 && step && step.length > 0 ) {
            qty = parseFloat(qty);
            step = parseFloat(step);
            if( qty < step ) {
                var diff = step - qty;
                var new_qty = qty + diff;
                $(this).val(new_qty);
                alert("This product can only be bought in multiples of " + step + ". It has been rounded up to the closest value.");
            } else if( qty > step && qty % step != 0 ) {
                // Round it up to step
                var new_qty = roundNearest(qty, step);
                $(this).val(new_qty);
                alert("This product can only be bought in multiples of " + step + ". It has been rounded up to the closest value.");
            }
        }
    });

    /**
     * Round to nearest
     * @param num
     * @param acc
     * @returns {number}
     */
    function roundNearest(num, acc){
        // if ( acc < 0 ) {
        //     return Math.round(num*acc)/acc;
        // } else {
        //     return Math.round(num/acc)*acc;
        // }
        var c = Math.round(num/acc) * acc;
        if( c <= num ) {
            c = c + acc;
        }
        return c;
    }

    // Category open/close
    if( $("ul.product-categories").length > 0 ) {

        // Category header toggle
        $(".category-header").on("click", function(ev) {
            ev.preventDefault();
            $("ul.product-categories").slideToggle();
        });

        // On Click
        $("ul.product-categories").find("li a").on("click", function(ev){
            var curItem = $(this).parent("li");
            if ( curItem.find("> ul").length > 0 ) {
                ev.preventDefault();
                $(curItem).find("> ul").slideToggle();
            }
        });

        // Show the active children
        var firstParent = $(".category.active").closest("ul.sub");
        var secondParent = firstParent.parents("ul.sub");
        setTimeout(function(){
            firstParent.slideDown();
            secondParent.slideDown();
        }, 500);

    }




    /**
     *  Prevent Default ADD TO CART, If
     */
    $('body').on('click', '.single_add_to_cart_button', function(event) {
        var prod_qty = parseInt($(".qty").val());
        var available_stock = parseInt($('.qty').attr('available_stock'));
        // alert("prod_qty => "+prod_qty+ " ,  available_stock => "+available_stock);

        if(prod_qty > available_stock){
            if (confirm(' Sorry, this item is currently out of stock. We will place this item on backorder for you.')) {
                // Do Nothing, Let Product add to the Cart
            } else {
                // Prevent Default ADD TO CART.
                event.preventDefault();
            }
        }
    });




});
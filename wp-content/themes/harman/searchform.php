<?php
/**
 * Template for displaying search forms in Harman
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>

<?php $unique_id = esc_attr( uniqid( 'search-form-' ) ); ?>

<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<input type="search" id="<?php echo $unique_id; ?>" class="search-field" placeholder="<?php echo esc_attr_x( 'Type and Hit Enter to Search&hellip;', 'placeholder', 'harman' ); ?>" value="<?php echo get_search_query(); ?>" name="s" />
    <input type="hidden" name="post_type" value="product" />
    <button type="submit" class="search-submit"><?php echo harman_get_svg( array( 'icon' => 'search' ) ); ?><span class="screen-reader-text"><?php echo _x( 'Search', 'submit button', 'harman' ); ?></span></button>
</form>

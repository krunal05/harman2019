<?php
/**
 * Template part for displaying events
 */

?>

<li id="event-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php if ( '' !== get_the_post_thumbnail() ) :
		$thumbnail_img = wp_get_attachment_image_src( get_post_thumbnail_id(), 'medium');
		$thumbnail_src = $thumbnail_img[0];
	else:
		$thumbnail_src = get_template_directory_uri() . '/assets/images/event-placeholder.jpg';
	endif; ?>

	<div class="event-thumb thumb">
		<a href="<?php the_permalink(); ?>">
			<img src="<?php echo $thumbnail_src; ?>" alt="Event Icon" />
		</a>
	</div><!-- .post-thumbnail -->

	<div class="event-content content">
		<header class="entry-header">
			<?php the_title( '<h3 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' ); ?>
		</header><!-- .entry-header -->
		<div class="entry-content">
			<?php the_content(); ?>
		</div>
		<a class="view" href="<?php the_permalink(); ?>" title="View Event">View Event</a>
	</div>

</li><!-- #post-## -->
<?php
/**
 * Displays top navigation
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>
<nav class="main-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Top Menu', 'harman' ); ?>">
	<?php wp_nav_menu( array(
        'menu' => 'Product Menu',
		'menu_id'        => '',
		'container'      => false,
        'depth' => '2',
	) ); ?>
</nav><!-- #site-navigation -->

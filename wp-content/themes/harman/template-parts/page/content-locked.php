<?php
/**
 * Template part for displaying locked content error
 */
?>
<div <?php post_class(); ?>>
	<section class="content-panel">
		<div class="container">
			<div class="content-title">
				<h1>Login Required</h1>
			</div>
			<div class="internal-content">
                <?php
                    $content = get_field('unauthorized_user_content');
                    echo $content;
                ?>
			</div>
		</div>
	</section>
</div>

<?php
/**
 * Template part for displaying page content in page
 */
?>
<div <?php post_class(); ?>>
    <section class="content-panel">
        <div class="container">
            <div class="content-title">
                <h1><?php echo get_the_title(); ?></h1>
            </div>
            <div class="internal-content">
		        <?php the_content(); ?>
            </div>
        </div>
    </section>
</div>

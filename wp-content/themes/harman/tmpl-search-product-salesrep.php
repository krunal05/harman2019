<?php
/**
 * Template Name: Sales Rep - Search Page
 */

$page_allowed = ["sales_rep","administrator"];
$loginCheck->page_access_to_roles($page_allowed);
get_header(); ?>

    <div  class="salesrep-search-page">
		<div id="primary" class="content-area">
			<main id="main" class="site-main" role="main">



                <div class="salesrep-search-form-wrapper">

                    <div class="keyword-search-wrapper">
                        <form class="keyword-search-form form" action="" method="post">
                            <input type="hidden" id="security-keyword-search" value="<?php echo wp_create_nonce('security-keyword-search-nonce'); ?>">
                            <div class="flex-wrapper">
                                <div class="input-wrapper select-wrapper">
                                    <h3>Search Keyword*</h3>
                                    <input type="text" name="keyword" id="keyword" class="required" value="">
                                </div> <!-- /input-wrapper -->

                                <div class="button-wrapper">
                                    <input type="button" value="Search" tabindex="5" id="keyword_search" name="keyword_search" class="thread-button button blue">
                                </div> <!-- /button-wrapper -->
                            </div>
                        </form>
                    </div>

                    <div class="range-search-wrapper">
                        <form class="range-search-form form"  action="">
                            <input type="hidden" id="security-range-search" value="<?php echo wp_create_nonce('security-range-search-nonce'); ?>">
                            <div class="flex-wrapper">
                                <div class="input-wrapper select-wrapper">
                                    <h3>Start SKU Range*</h3>
                                    <input type="text" name="start-range-sku" class="required"  value="">
                                </div> <!-- /input-wrapper -->

                                <div class="input-wrapper select-wrapper">
                                    <h3>End SKU Range*</h3>
                                    <input type="text" name="end-range-sku" class="required" value="">
                                </div> <!-- /input-wrapper -->

                                <div class="button-wrapper">
                                    <input type="button" value="Search" tabindex="5" id="range_search" name="range_search" class="thread-button button blue">
                                </div> <!-- /button-wrapper -->
                            </div>
                        </form>
                    </div>

                </div>



                <div class="salesrep-search-result-wrapper">




                </div>



			</main><!-- #main -->
		</div><!-- #primary -->
	</div>

<?php get_footer();

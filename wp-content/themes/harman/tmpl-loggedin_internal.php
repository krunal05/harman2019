<?php
/**
 * Template Name: Logged In Only
 */

get_header(); ?>

<?php if( is_user_logged_in() ) { ?>
	<div id="internal-wrap">
<?php } else { ?>
	<div id="page-wrap">
<?php } ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();
				if( is_user_logged_in() ) {
					get_template_part( 'template-parts/page/content', 'internal' );
				} else {
					get_template_part( 'template-parts/page/content', 'locked' );
				}
			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->
	</div>

<?php get_footer();

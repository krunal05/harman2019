<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<?php
$thumbnail = get_template_directory_uri() . '/assets/images/woods.jpg';
?>
    <div id="internal-wrap">
        <div id="primary" class="content-area">
            <main id="main" class="site-main" role="main">

                <div class="container">
                    <div class="internal-content-wrap">
                        <div class="internal-content" style="text-align: center;">
                            <h1><?php _e( 'Oops! Nothing found here.', 'harman' ); ?></h1>
                        </div>
                    </div>
                </div>

            </main><!-- #main -->
        </div><!-- #primary -->
    </div>

<?php get_footer();

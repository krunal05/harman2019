<?php

/**
 * Filter query for search
 * @param $query
 * @param bool $error
 */
function gp_filter_query( $query, $error = true ) {

	if ( is_search() ) {
		$query->is_search = false;
		$query->query_vars[s] = false;
		$query->query[s] = false;

		if ( $error == true )
			$query->is_404 = true;

	}
}
//add_action( 'parse_query', 'gp_filter_query' );
//add_filter( 'get_search_form', create_function( '$a', "return null;" ) );

/**
 * Add title attribute to custom logo
 * @param $html
 * @param $blog_id
 * @return mixed
 */
function gp_get_custom_logo( $html, $blog_id ) {

	if( !empty( $html ) ) {
		$title = get_bloginfo();
		$html = str_replace( 'class="custom-logo-link"', sprintf('class="custom-logo-link" title="%s"', $title), $html );
	}

	return $html;
}
add_filter('get_custom_logo', 'gp_get_custom_logo', 10, 2);

/**
 * Returns the post thumbnail src
 * @param string $size
 * @return string
 */
function gp_get_post_thumbnail_src( $size = 'full' ) {

	// Default post
	$post = null;
	$post = get_post( $post );
	if ( ! $post ) {
		return '';
	}

	// Get the id
	$post_thumbnail_id = get_post_thumbnail_id( $post );
	if( $post_thumbnail_id ) {
		$image = wp_get_attachment_image_src( $post_thumbnail_id, $size );
		if( isset( $image[0] ) ) {
			return $image[0];
		}
	}

	// Return
	return '';
}

/**
 * Returns the query posts (custom/page/post)
 * @param $options
 * @return array|bool
 */
function gp_get_posts( $options ) {

	// Default
	$options = array_merge(
		array(
			'post_type'      => 'post',
			'posts_per_page' => 5,
			'orderby'        => 'menu_order',
			'order'          => 'ASC'
		), $options
	);

	// Args
	$args = array(
		'post_type'      => $options['post_type'],
		'posts_per_page' => $options['posts_per_page'],
		'orderby'        => $options['orderby'],
		'order'          => $options['order'],
	);

	// Query
	$query = new WP_Query( $args );

	// Results
	return $query;

}

/**
 * Enqueues scripts and styles.
 */
function gp_enqueue_scripts() {

	// Add the minimized stylesheet
	wp_enqueue_style( 'geekpower-styles', get_template_directory_uri() . '/assets/css/style.min.css' );

	// Add jquery
	wp_enqueue_script( 'jquery-script', get_template_directory_uri() . '/assets/js/jquery-3.2.1.min.js' );

	// Add font awesome
	wp_enqueue_style( 'fontawesome-styles', get_template_directory_uri() . '/assets/font-awesome/css/font-awesome.min.css' );

	// Add Moby
	wp_enqueue_style( 'moby-styles', get_template_directory_uri() . '/assets/css/moby.min.css' );
	wp_enqueue_script( 'moby-script', get_template_directory_uri() . '/assets/js/moby.min.js' );

	wp_enqueue_style( 'slick-styles', get_template_directory_uri() . '/assets/css/slick.css' );
	wp_enqueue_script( 'slick-script', get_template_directory_uri() . '/assets/js/slick.min.js' );

	// Register
	wp_register_script('geekpower-script', get_template_directory_uri() . '/assets/js/main.min.js' );

	// Localize any vars to the main script
	harman_woo_localize_vars('geekpower-script');

	// Add minimized js
	wp_enqueue_script( 'geekpower-script');

}
add_action( 'wp_enqueue_scripts', 'gp_enqueue_scripts' );
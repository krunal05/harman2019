<?php
/**
 * Register custom post types
 */
function gp_register_custom_post_types() {

	// Register rotator
	$labels = array(
		'name'               => _x( 'Rotator', 'post type general name', 'harman' ),
		'singular_name'      => _x( 'Item', 'post type singular name', 'harman' ),
		'menu_name'          => _x( 'Rotator', 'admin menu', 'harman' ),
		'name_admin_bar'     => _x( 'Item', 'add new on admin bar', 'harman' ),
		'add_new'            => _x( 'Add New', 'book', 'harman' ),
		'add_new_item'       => __( 'Add New Item', 'harman' ),
		'new_item'           => __( 'New Item', 'harman' ),
		'edit_item'          => __( 'Edit Item', 'harman' ),
		'view_item'          => __( 'View Item', 'harman' ),
		'all_items'          => __( 'All Items', 'harman' ),
		'search_items'       => __( 'Search Items', 'harman' ),
		'parent_item_colon'  => __( 'Parent Items:', 'harman' ),
		'not_found'          => __( 'No items found.', 'harman' ),
		'not_found_in_trash' => __( 'No items found in Trash.', 'harman' )
	);

	$args = array(
		'labels'             => $labels,
		'public'             => false,
		'publicly_queryable' => false,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => false,
		'capability_type'    => 'post',
		'has_archive'        => false,
		'hierarchical'       => false,
		'menu_position'      => 107,
		'supports'           => array( 'title', 'editor', 'thumbnail' )
	);

	register_post_type( 'rotator', $args );

	// Register events
	$labels = array(
		'name'               => _x( 'Events', 'post type general name', 'harman' ),
		'singular_name'      => _x( 'Item', 'post type singular name', 'harman' ),
		'menu_name'          => _x( 'Events', 'admin menu', 'harman' ),
		'name_admin_bar'     => _x( 'Item', 'add new on admin bar', 'harman' ),
		'add_new'            => _x( 'Add New', 'book', 'harman' ),
		'add_new_item'       => __( 'Add New Item', 'harman' ),
		'new_item'           => __( 'New Item', 'harman' ),
		'edit_item'          => __( 'Edit Item', 'harman' ),
		'view_item'          => __( 'View Item', 'harman' ),
		'all_items'          => __( 'All Items', 'harman' ),
		'search_items'       => __( 'Search Items', 'harman' ),
		'parent_item_colon'  => __( 'Parent Items:', 'harman' ),
		'not_found'          => __( 'No items found.', 'harman' ),
		'not_found_in_trash' => __( 'No items found in Trash.', 'harman' )
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => false,
		'capability_type'    => 'post',
		'rewrite'            => array( 'slug' => 'events' ),
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => 108,
		'supports'           => array( 'title', 'editor', 'thumbnail' )
	);

	register_post_type( 'events', $args );

	// Register Labels
	$labels = array(
		'name'               => _x( 'Labels', 'post type general name', 'harman' ),
		'singular_name'      => _x( 'Item', 'post type singular name', 'harman' ),
		'menu_name'          => _x( 'Labels', 'admin menu', 'harman' ),
		'name_admin_bar'     => _x( 'Item', 'add new on admin bar', 'harman' ),
		'add_new'            => _x( 'Add New', 'book', 'harman' ),
		'add_new_item'       => __( 'Add New Item', 'harman' ),
		'new_item'           => __( 'New Item', 'harman' ),
		'edit_item'          => __( 'Edit Item', 'harman' ),
		'view_item'          => __( 'View Item', 'harman' ),
		'all_items'          => __( 'All Items', 'harman' ),
		'search_items'       => __( 'Search Items', 'harman' ),
		'parent_item_colon'  => __( 'Parent Items:', 'harman' ),
		'not_found'          => __( 'No items found.', 'harman' ),
		'not_found_in_trash' => __( 'No items found in Trash.', 'harman' )
	);

	$args = array(
		'labels'             => $labels,
		'public'             => false,
		'publicly_queryable' => false,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => false,
		'capability_type'    => 'post',
		'has_archive'        => false,
		'hierarchical'       => false,
		'menu_position'      => 109,
		'supports'           => array( 'title', 'thumbnail' )
	);

	register_post_type( 'about-labels', $args );

}
add_action('init', 'gp_register_custom_post_types' );
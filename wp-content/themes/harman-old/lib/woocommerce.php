<?php
/**
 * Handles all Woocommerce functions and hooks
 */

// ************ REMOVED HOOKS *********** //
// Unhook the Woocommerce wrappers
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);
// Remove ordering
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
// Remove the Woocommerce sidebar
remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10 );
// Remove Woocommerce tabs
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
// Remove single product excerpt
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
// Remove single product rating
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 15 );
// Remove the wc notice to move it after navigation
remove_action( 'woocommerce_before_shop_loop', 'wc_print_notices', 10 );
// Remove product title from archive
remove_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10 );
// Remove single product meta
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
// Remove open link and close links
remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 );
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5 );
// Remove archive pricing
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );

// ************ ADDED HOOKS *********** //

add_action( 'woocommerce_before_main_content', 'harman_wrapper_start', 10 );
add_action( 'woocommerce_before_main_content', 'harman_nav_wrapper_start', 40 );
add_action( 'woocommerce_after_main_content', 'harman_wrapper_end', 10 );
// Add nav wrapper end
add_action( 'woocommerce_before_shop_loop', 'harman_nav_wrapper_end', 40 );
add_action( 'woocommerce_no_products_found', 'harman_nav_wrapper_end', 1 );
// Add wc notices back
add_action( 'woocommerce_before_shop_loop', 'wc_print_notices', 50 );
// Update breadcrumbs
add_filter( 'woocommerce_breadcrumb_defaults', 'harman_woo_breadcrumb_defaults', 10 );
// Products holder
add_action( 'woocommerce_before_shop_loop', 'harman_woo_product_wrapper_start', 60 );
add_action( 'woocommerce_no_products_found', 'harman_woo_product_wrapper_start', 2 );
add_action( 'woocommerce_after_shop_loop', 'harman_woo_product_wrapper_end', 20 );
add_action( 'woocommerce_no_products_found', 'harman_woo_product_wrapper_end', 20 );
// Add categories to product archive and no products
add_filter( 'woocommerce_product_loop_start', 'harman_woo_add_categories_html', 10 );
add_action( 'woocommerce_no_products_found', 'harman_woo_no_product_categories_html', 3 );

// Build archive product title
add_action( 'woocommerce_before_shop_loop_item', 'harman_woo_shop_loop_item_wrapper_start', 5 );
add_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_title', 6 );
add_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_price', 15 );
add_action( 'woocommerce_before_shop_loop_item', 'harman_woo_shop_loop_item_wrapper_end', 20 );
// Open/close archive thumbnail after product info
add_action( 'woocommerce_before_shop_loop_item_title', 'harman_woo_shop_loop_item_thumb_wrapper_start', 1 );
add_action( 'woocommerce_after_shop_loop_item', 'harman_woo_shop_loop_item_thumb_wrapper_end', 50 );
// Build archive product cart
add_action( 'woocommerce_after_shop_loop_item', 'harman_woo_shop_loop_item_cart_wrapper_start', 7);
add_action( 'woocommerce_after_shop_loop_item', 'harman_woo_shop_loop_item_cart_wrapper_end', 20);
// Not found wrap
add_action( 'woocommerce_no_products_found', 'harman_woo_no_products_found_wrapper_start', 9 );
add_action( 'woocommerce_no_products_found', 'harman_woo_no_products_found_wrapper_end', 15 );
// Update related products columns
add_filter( 'woocommerce_related_products_columns', 'harman_woo_related_products_columns', 10 );
// Add custom fields to Products
add_action( 'woocommerce_product_options_general_product_data', 'harman_woo_product_options_general_product_data', 10 );
// Add custom pricing fields to products pricing
add_action( 'woocommerce_product_options_pricing', 'harman_woo_product_options_pricing', 10 );
// Add custom pricing fields to variable products
add_action( 'woocommerce_variation_options_pricing', 'harman_woo_variation_options_pricing', 10, 3 );
// Save custom fields to products
add_action( 'woocommerce_process_product_meta', 'harman_woo_process_product_meta', 10 );
// Save custom fields for products variations
add_action( 'woocommerce_save_product_variation', 'harman_woo_save_product_variation', 10, 2 );
// Add custom fields to frontend
add_action( 'woocommerce_single_product_summary', 'harman_woo_product_sku_html', 6 );
// Add Product description to product
add_action( 'woocommerce_single_product_summary', 'harman_woo_product_description', 20 );
// After add to cart
add_action( 'woocommerce_after_add_to_cart_button', 'harman_woo_after_add_to_cart_button', 10 );
// Update gallery thumbnail column
add_filter( 'woocommerce_product_thumbnails_columns', 'harman_woo_product_thumbnails_columns', 10 );
// Update gallery thumbnail width
add_filter( 'woocommerce_get_image_size_gallery_thumbnail', 'harman_woo_get_image_size_gallery_thumbnail', 10 );
// Roles
add_action( 'after_switch_theme', 'harman_woo_add_user_role' );
// Autodetect User currency
add_action( 'init', 'harman_woo_autodetect_currency' );
// No Add to cart
//add_filter( 'woocommerce_is_purchasable', 'harman_woo_is_purchasable', 10 );
// Filter add to cart args
add_filter( 'woocommerce_loop_add_to_cart_args', 'harman_woo_loop_add_to_cart_args', 10, 2 );
// Filter out prices depending on options
add_filter( 'woocommerce_get_price_html', 'harman_woo_get_price_html', 10, 2 );
// Suffix to price
add_filter( 'woocommerce_get_price_suffix', 'harman_woo_get_price_suffix', 10, 4 );
// Price Filters
add_filter( 'woocommerce_product_get_price', 'harman_woo_product_get_price', 10, 2 );
add_filter( 'woocommerce_product_variation_get_price', 'harman_woo_product_get_price', 10, 2 );
add_filter( 'woocommerce_product_get_regular_price', 'harman_woo_product_get_regular_price', 10, 2 );
add_filter( 'woocommerce_product_variation_get_regular_price', 'harman_woo_product_get_regular_price', 10, 2 );
add_filter( 'woocommerce_product_get_sale_price', 'harman_woo_product_get_sale_price', 10, 2 );
add_filter( 'woocommerce_product_variation_get_sale_price', 'harman_woo_product_get_sale_price', 10, 2 );
add_filter( 'woocommerce_variation_prices_price', 'harman_woo_product_get_price', 10, 2 );
add_filter( 'woocommerce_variation_prices_regular_price', 'harman_woo_product_get_regular_price', 10, 2 );
add_filter( 'woocommerce_variation_prices_sale_price', 'harman_woo_product_get_sale_price', 10, 2 );
add_filter( 'single_product_archive_thumbnail_size', 'harman_woo_single_product_archive_thumbnail_size', 10 );
// Variations Hash for handling min/max prices
add_filter( 'woocommerce_get_variation_prices_hash', 'harman_woo_get_variation_prices_hash', 10, 3 );
add_filter( 'woocommerce_variation_is_visible', 'harman_woo_variation_is_visible', 10, 4 );
// Prevent add to cart
add_filter( 'woocommerce_add_to_cart_validation', 'harman_woo_add_to_cart', 10, 4 );
add_filter( 'woocommerce_update_cart_validation', 'harman_woo_update_cart_validation', 5, 5 );
// Check items in cart for min order quantity
add_action( 'woocommerce_check_cart_items', 'harman_woo_check_cart_items' );
// Min add to cart ajax
add_filter( 'woocommerce_loop_add_to_cart_link', 'harman_woo_add_to_cart_link', 10, 2 );
// Minimum cart amount
add_action( 'woocommerce_before_cart' , 'harman_woo_minimum_order_amount' );
add_action( 'woocommerce_checkout_process', 'harman_woo_minimum_order_amount' );
// Availability text
add_filter( 'woocommerce_get_availability_text', 'harman_woo_get_availability_text', 10, 2 );
// Cart input box variable filters
add_filter( 'woocommerce_quantity_input_min', 'harman_woo_quantity_input_min_value', 1, 2 );
add_filter( 'woocommerce_quantity_input_step', 'harman_woo_quantity_input_step_value', 1, 2 );
add_filter( 'woocommerce_quantity_input_args', 'harman_woo_quantity_input_args_values', 1, 2 );
add_filter( 'woocommerce_available_variation', 'harman_woo_available_variation', 10, 3 );

//Stock / Inventory change

//add_filter( 'woocommerce_product_backorders_allowed', 'harman_woo_product_backorders_allowed', 10, 3 );
//add_filter( 'woocommerce_product_backorders_require_notification', 'harman_woo_product_backorders_require_notification', 10, 2 );
//add_filter( 'woocommerce_product_is_in_stock', 'harman_woo_product_is_in_stock', 10, 2 );


// Search by sku
add_action( 'woocommerce_product_query', 'harman_woo_update_product_query', 10, 2 );
add_filter( 'woocommerce_get_script_data', 'harman_woo_get_script_data', 10, 2 );

//Redirect From all woocommerce page if User is not logged in
//add_action('template_redirect', 'harman_woo_user_redirect');

// ************ Hooks Callbacks *********** //

function harman_woo_user_redirect() {
    if (! is_user_logged_in() && (is_woocommerce() || is_cart() || is_checkout())) {
        wp_redirect(site_url()."/my-account/"); // if user not logged in redirect them to Home page. if you want to redirect on any other page set Url your destination page.
        exit;
    }
}

/**
 * Filters the unavailable variation text
 * @param $params
 * @param $handle
 * @return mixed
 */
function harman_woo_get_script_data($params, $handle) {

    if( $handle === "wc-add-to-cart-variation" && isset( $params['i18n_unavailable_text'] ) && !is_user_logged_in() ) {
	    $params['i18n_unavailable_text'] = esc_attr__( 'Please login or register to complete a purchase.', 'woocommerce' );
    }
    return $params;
}

/**
 * Search product data for a string and return ids.
 * @param $s
 * @return array
 */
function harman_woo_search_products_by_sku($s) {
    global $wpdb;

	$s = '%' . $wpdb->esc_like( $s ) . '%';
	$post_types = array( 'product', 'product_variation' );
	$post_statuses = array( 'publish' );
	$search_results = $wpdb->get_results(
		$wpdb->prepare(
			"SELECT DISTINCT posts.ID as product_id, posts.post_parent as parent_id FROM {$wpdb->posts} posts
				LEFT JOIN {$wpdb->postmeta} postmeta ON posts.ID = postmeta.post_id
				WHERE postmeta.meta_key = '_sku' AND postmeta.meta_value LIKE %s
				    AND posts.post_type IN ('" . implode( "','", $post_types ) . "')
                    AND posts.post_status IN ('" . implode( "','", $post_statuses ) . "') 
				ORDER BY posts.post_parent ASC, posts.post_title ASC",
			$s
		)
	);
	$product_ids = wp_parse_id_list( array_merge( wp_list_pluck( $search_results, 'product_id' ), wp_list_pluck( $search_results, 'parent_id' ) ) );
	return wp_parse_id_list( array_filter($product_ids) );
}

/**
 * Adds sku to product search
 * @param $q
 * @param $wc_query
 */
function harman_woo_update_product_query($q, $wc_query) {

    $search = wc_clean( $q->get('s') );
	$ids = harman_woo_search_products_by_sku( $search );
	if( !empty($ids) ) {
		$q->set('post__in', $ids);
		$q->set('s', '');
	}

}




/**
 * Filter product in stock
 * @param $instock
 * @param $product
 * @return bool
 */
function harman_woo_product_is_in_stock($instock, $product) {

    // Always instock for logged in users
    if( is_user_logged_in() && $product->get_backorders() == "no" && !$instock && !is_admin() ) {
	    return true;
    }

    return $instock;
}

/**
 * Filter to disable backorder notification
 * @param $default
 * @param $product
 * @return bool
 */
function harman_woo_product_backorders_require_notification($default, $product) {
    return false;
}

/**
 * Filter to allow backorders without notification
 * @param $allowed
 * @param $id
 * @param $product
 * @return bool
 */
function harman_woo_product_backorders_allowed($allowed, $id, $product) {

    if( is_user_logged_in() ) {
        return true;
    }
    return $allowed;
}

/**
 * Filters out the available variation args
 * @param $args
 * @param $product
 * @param $variation
 * @return mixed
 */
function harman_woo_available_variation($args, $product, $variation) {

	// Rule can apply
	if( !harman_woo_min_quantity_rule_applies() ) {
		return $args;
	}

	$min_quantity = absint( get_post_meta( $variation->get_id(), 'min_order', true ) );
	if( $min_quantity && $min_quantity > 0 ) {
		$args['min_qty'] = $min_quantity;
		$args['step'] = $min_quantity;
	}

    if ( $variation->managing_stock() ){
        $args['available_stock'] = $variation->get_stock_quantity();
        if($args['available_stock'] == ""){
            $args['available_stock'] = 0;
        }
    }
	// Return
	return $args;

}

/**
 * Cart Update Validation to ensure quantity ordered follows the wholesale rules
 * @param $passed
 * @param $cart_item_key
 * @param $values
 * @param $quantity
 * @return mixed
 */
function harman_woo_update_cart_validation($passed, $cart_item_key, $values, $quantity) {

	// Rule can apply
	if( !harman_woo_min_quantity_rule_applies() ) {
		return $passed;
	}

	$product = $values['data'];

	if( $values['variation_id'] ) {
		$id = $values['variation_id'];
		$min_quantity = absint( get_post_meta( $id, 'min_order', true ) );
	} else {
		$id = $values['product_id'];
		$min_quantity = absint( get_post_meta( $id, 'min_order', true ) );
	}

	if( $min_quantity > 0 && $quantity < $min_quantity ) {
		$error = sprintf( __( 'The minimum allowed quantity for %s is %d - please increase the quantity in your cart', 'harman' ), $product->get_title(), $min_quantity );
		wc_add_notice( $error, 'error' );
		return false;
	}

	// Check for validation
	if( $min_quantity > 0 && $quantity % $min_quantity != 0 ) {
		$error = sprintf( __( "You may only add %s in multiples of %d to your cart.", 'harman' ), $product->get_title(), $min_quantity );
		wc_add_notice( $error, 'error' );
		return false;
	}

    return $passed;

}

/**
 * Filters the availability text for product
 * @param $availability
 * @param $product
 * @return string
 */
function harman_woo_get_availability_text($availability, $product) {
    // No "in stock" text
	if ( $product->managing_stock() ) {
	    $availability = '';
	}
    return $availability;
}

/**
 * Checks the cart and checkout for minimum order
 */
function harman_woo_minimum_order_amount()
{

    // Minimum order value
    $minimum = 300;
    $new_customers_minimum = $minimum;

    // Are there any previous orders
    if( is_user_logged_in() && harman_user_has_bought() ) {
        $minimum = 300;
    }

    if (WC()->cart->total < $minimum) {

        if (is_cart()) {

            wc_print_notice(
                sprintf('Minimum order for new customers is %s. All reorders following require a minimum purchase of $300.',
                    wc_price($new_customers_minimum)
                ), 'error'
            );

        } else {

            wc_add_notice(
                sprintf('You must have an order with a minimum of %s to place your order, your current order total is %s.',
                    wc_price($minimum),
                    wc_price(WC()->cart->total)
                ), 'error'
            );

        }

    }
}

/**
 * Filter Step, Min and Max for Quantity Input Boxes on product pages
 * @param $args
 * @param $product
 * @return mixed
 */
function harman_woo_quantity_input_args_values($args, $product) {

	// Rule can apply
	if( !harman_woo_min_quantity_rule_applies() ) {
		return $args;
	}

	// Get the minimum qty to order
	$minimum_quantity   = absint( get_post_meta( $product->get_id(), 'min_order', true ) );
	if( $minimum_quantity && $minimum_quantity > 0 ) {
		$args['min_value'] = $minimum_quantity;
		$args['step'] = $minimum_quantity;
	}

    if ( $product->managing_stock() ){
         $args['available_stock'] = $product->get_stock_quantity();
         if($args['available_stock'] == ""){
             $args['available_stock'] = 0;
         }

    }

	// Return
	return $args;

}

/**
 * Filter Step quantity value for input boxes for cart
 * @param $default
 * @param $product
 * @return int
 */
function harman_woo_quantity_input_step_value($default, $product) {

	// Return Defaults if it isn't a simple product
	if( $product->product_type != 'simple' ) {
		return $default;
	}

	// Rule can apply
	if( !harman_woo_min_quantity_rule_applies() ) {
		return $default;
	}

	// Get the minimum qty to order
	$minimum_quantity   = absint( get_post_meta( $product->id, 'min_order', true ) );
	if( $minimum_quantity && $minimum_quantity > 0 ) {
		return $minimum_quantity;
	}

	return $default;

}

/**
 * Filters minimum quantity value for input boxes for cart
 * @param $default
 * @param $product
 * @return int
 */
function harman_woo_quantity_input_min_value($default, $product) {

	// Return Defaults if it isn't a simple product
	if( $product->product_type != 'simple' ) {
		return $default;
	}

	// Rule can apply
	if( !harman_woo_min_quantity_rule_applies() ) {
		return $default;
	}

	// Get the minimum qty to order
	$minimum_quantity   = absint( get_post_meta( $product->id, 'min_order', true ) );
	if( $minimum_quantity && $minimum_quantity > 0 ) {
		return $minimum_quantity;
	}

	return $default;

}

/**
 * Add quantity property to add to cart button on shop loop for simple products.
 * @param $html
 * @param $product
 * @return mixed
 */
function harman_woo_add_to_cart_link( $html, $product ) {

	// Rule can apply
	if( !harman_woo_min_quantity_rule_applies() ) {
		return $html;
	}

	if ( 'variable' !== $product->product_type ) {
		$minimum_quantity   = absint( get_post_meta( $product->id, 'min_order', true ) );
		if( $minimum_quantity && $minimum_quantity > 0 ) {
			$html = str_replace( '<a ', '<a data-quantity="' . $minimum_quantity . '" ', $html );
        }
    }

    return $html;
}

/**
 * Validate cart items for min order
 */
function harman_woo_check_cart_items() {

    // Rule can apply
    if( !harman_woo_min_quantity_rule_applies() ) {
        return;
    }

    // Default
	$product_quantity = array();
	// Count items + variations first
	foreach ( WC()->cart->get_cart() as $cart_item_key => $values ) {

		$product = $values['data'];

		if( $values['variation_id'] ) {
		    $id = $values['variation_id'];
			$min_quantity = absint( get_post_meta( $values['variation_id'], 'min_order', true ) );
		} else {
			$id = $values['product_id'];
			$min_quantity = absint( get_post_meta( $values['product_id'], 'min_order', true ) );
		}

		if( !isset( $product_quantity[$id] ) ) {
		    $product_quantity[$id] = $values['quantity'];
        } else {
			$product_quantity[$id] += $values['quantity'];
        }

        if( $min_quantity > 0 && $product_quantity[$id] < $min_quantity ) {
	        $error = sprintf( __( 'The minimum allowed quantity for %s is %d - please increase the quantity in your cart', 'harman' ), $product->get_title(), $min_quantity );
	        wc_add_notice( $error, 'error' );
	        continue;
        }

		// Check for validation
		if( $min_quantity > 0 && $product_quantity[$id] % $min_quantity != 0 ) {
			$error = sprintf( __( "You may only add %s in multiples of %d to your cart.", 'harman' ), $product->get_title(), $min_quantity );
			wc_add_notice( $error, 'error' );
			continue;
		}

	}

}

/**
 * Add to cart validation
 * @access public
 * @param mixed $pass
 * @param mixed $product_id
 * @param mixed $quantity
 * @return void
 */
function harman_woo_add_to_cart( $pass, $product_id, $quantity, $variation_id = 0 ) {

	// Rule can apply
	if( !harman_woo_min_quantity_rule_applies() ) {
		return $pass;
	}

    // Check for min quantity rule
    $has_minimum_rule = false;
	if ( $variation_id ) {
		$min_quantity = get_post_meta($variation_id, 'min_order', true);
	} else {
		$min_quantity = get_post_meta($product_id, 'min_order', true);
    }

    // There is minimum quantity rule
    if( !empty($min_quantity) && $min_quantity > 0 ) {

	    // Check for validation
        if( $quantity % $min_quantity != 0 ) {
	        $product = wc_get_product($product_id);
	        $error = sprintf( __( "You may only add %s in multiples of %d to your cart.", 'harman' ), $product->get_title(), $min_quantity );
	        wc_add_notice( $error, 'error' );
	        return false;
        }

	    $has_minimum_rule = true;
    }

    // Quantity added
	$total_quantity = $quantity;
	// Count items from cart already
	foreach ( WC()->cart->get_cart() as $cart_item_key => $values ) {

		if ( $has_minimum_rule ) {
			if ( $values['variation_id'] && $values['variation_id'] == $variation_id ) {
				$total_quantity += $values['quantity'];
			} else if ( $values['product_id'] == $product_id ) {
				$total_quantity += $values['quantity'];
			}
		}
	}

    if( $total_quantity < $min_quantity ) {
	    $pass = false;
	    $error = sprintf( __( 'The minimum allowed quantity for this product is %d.', 'harman' ), $min_quantity );
	    wc_add_notice( $error, 'error' );
    }

    return $pass;
}

/**
 * Filter out whether to show variation with empty price
 * @param $visible
 * @param $variation_id
 * @param $product_id
 * @param $variation
 * @return bool
 */
function harman_woo_variation_is_visible($visible, $variation_id, $product_id, $variation) {
	if( !is_user_logged_in() ) {
		return 'publish' === get_post_status( $variation_id );
	}
	return $visible;
}

/**
 * Overwrite woocommerce function to get loop title
 */
function woocommerce_template_loop_product_title() {
	echo '<h2 class="woocommerce-loop-product__title"><a href="'. get_permalink() .'" title="View Product">' . get_the_title() . '</a></h2>';
}

/**
 * Filters the archive thumbnail size
 * @param $size
 * @return string
 */
function harman_woo_single_product_archive_thumbnail_size($size) {
    $size = 'full';
    return $size;
}

/**
 * Echoes the wrapper start for thumbnails
 */
function harman_woo_shop_loop_item_thumb_wrapper_start() {
    echo '<div class="thumb-holder">';
}

/**
 * Echoes the wrapper closing div for thumbnails
 */
function harman_woo_shop_loop_item_thumb_wrapper_end() {
    echo '</div>';
}

/**
 * Updates the variation price hash
 * @param $hash
 * @param $product
 * @param $display
 * @return array
 */
function harman_woo_get_variation_prices_hash( $hash, $product, $display ) {

	// Not logged in
	if( !is_user_logged_in() ) {
		return $hash;
	}

	$currency = harman_woo_get_current_currency();
	$user = wp_get_current_user();

	// Add role prefix for customers
	$role_prefix = in_array('customer', $user->roles) ? '_retail' : '';

	if( !empty($role_prefix) ) {
	    $priceField = $currency === "USD" ? $role_prefix . '_us_price' : $role_prefix . '_regular_price';
    } else {
	    $priceField = $currency === "USD" ? '_us_price' : '';
    }

    if( !empty($priceField) ) {
	    $hash[] = array(
		    $priceField
	    );
    }

    return $hash;

}

/**
 * Filter out the price for display
 * @param $price
 * @param $product WC_Product
 * @return mixed|string
 */
function harman_woo_product_get_price($price, $product) {

	// Logged in users
	if( is_user_logged_in() ) {

		// Current selected currency
		$currency = harman_woo_get_current_currency();
		$user = wp_get_current_user();

		// Add role prefix for customers
		$role_prefix = in_array('customer', $user->roles) ? '_retail' : '';

		// Is is on sale
		if( $product->is_on_sale() ) {
			$priceField = $currency === "USD" ? $role_prefix . '_us_sale_price' : $role_prefix . '_sale_price';
        } else {
			$priceField = $currency === "USD" ? $role_prefix . '_us_price' : $role_prefix . '_price';
        }

        // Get price to show
		$price = get_post_meta( $product->get_id(), $priceField, true );

		// No price set
		if( empty( $price ) || is_null($price) ) {
			$price = '';
		}

	} else {
		$price = '';
	}


	// Return
	return $price;
}

/**
 * Filter out the sale prices for simple product depending on location
 * @param $price
 * @param $product
 * @return mixed|string
 */
function harman_woo_product_get_sale_price($price, $product) {

	// Logged in users
	if( is_user_logged_in() ) {
		global $current_user;

		$currency = harman_woo_get_current_currency();
		$user = wp_get_current_user();

		// Add role prefix for customers
		$role_prefix = in_array('customer', $user->roles) ? '_retail' : '';

		// Get price field
		$priceField = $currency === "USD" ? $role_prefix . '_us_sale_price' : $role_prefix . '_sale_price';

		// Get price to show
		$price = get_post_meta( $product->get_id(), $priceField, true );

		// No price set
		if( empty( $price ) || is_null($price) ) {
			$price = '';
		}

	} else {
		$price = '';
	}

	// Return
	return $price;

}

/**
 * Filter out the prices for simple product depending on location
 * @param $price
 * @param $product
 * @return mixed|string
 */
function harman_woo_product_get_regular_price($price, $product) {

	// Logged in users
	if( is_user_logged_in() ) {

		$currency = harman_woo_get_current_currency();
		$user = wp_get_current_user();

		// Add role prefix for customers
		$role_prefix = in_array('customer', $user->roles) ? '_retail' : '';

		// Get price field
		$priceField = $currency === "USD" ? $role_prefix . '_us_price' : $role_prefix . '_regular_price';

		// Get price to show
		$price = get_post_meta( $product->get_id(), $priceField, true );

		// No price set
		if( empty( $price ) || is_null($price) ) {
			$price = '';
		}

	} else {
		$price = '';
	}

	// Return
    return $price;
}

/**
 * Filters the price suffix html
 * @param $html
 * @param $product
 * @param $price
 * @param $qty
 * @return string
 */
function harman_woo_get_price_suffix( $html, $product, $price, $qty ) {
    if( empty($html) && !is_admin() ) {
        $currency = '<span class="currency">'.harman_woo_get_current_currency().'</span>';
        $html = $currency;
    }
    return $html;
}

/**
 * Filter price html
 * @param $price_html
 * @param $product
 * @return string
 */
function harman_woo_get_price_html($price_html, $product) {

    // Users need to be logged in to see prices
    if( is_user_logged_in() ) {
        return $price_html;
    }

	return '';
}

/**
 * Update class on loop product args
 * @param $args
 * @param $product
 * @return mixed
 */
function harman_woo_loop_add_to_cart_args( $args, $product ) {
    $classes = explode(' ', $args['class']);
    // Not purchasable
    if( !$product->is_purchasable() ) {
        $classes[] = 'read-more-button';
        // Remove ajax add to cart
        if( $key = array_search( 'ajax_add_to_cart', $classes ) ) {
            unset($classes[$key]);
        }
    }
    // Update classes
    $args['class'] = implode(' ', $classes);
    return $args;
}

/**
 * Filters products purchasable option
 * @return bool
 */
function harman_woo_is_purchasable( $is_purchasable ) {
    return false; // NO Add to cart on launch
}

/**
 * Adds user role on theme switch
 */
function harman_woo_add_user_role() {

	// Set the user role info
	$role = 'wholesale';
	$role_title =  __( 'Wholesale User' );
	$role_caps =  array(
		'read'    => true,
		'level_0' => true,
	);

    // Don't do anything if wholesale user exists
    if( !get_role($role) ) {
	    // Remove if already exists
	    remove_role( $role );
	    // Now add with new compatibility
	    add_role( $role, $role_title, $role_caps );
    }

}

/**
 * Filters the gallery thumbnail size
 * @param $size
 * @return string
 */
function harman_woo_get_image_size_gallery_thumbnail($size) {
	$size['width']  = 150;
	$size['height'] = $size['width'];
	$size['crop']   = 1;
	return $size;
}
/**
 * Filters the number of columns to be shown for gallery thumbnails
 * @param $columns
 * @return int
 */
function harman_woo_product_thumbnails_columns($columns) {
	return 3;
}

/**
 * After add to cart button
 */
function harman_woo_after_add_to_cart_button() {
	global $product;

	// Get terms
	$terms = get_the_terms($product->get_id(), 'product_cat');
	if ( is_wp_error( $terms ) || empty( $terms ) ) {
		return;
	}

	// Grab first
	$term = current($terms);
	?>
	<a class="button cat-button" href="<?php echo esc_url(get_term_link($term)); ?>" title="View products in this category">View All In Category</a>
	<?php

}

/**
 * Renders the product description
 */
function harman_woo_product_description() {
	global $post;

	$gs_stamp = get_post_meta($post->ID, '_gs_stamp', true);
	?>
	<div class="product-description<?php echo !empty($gs_stamp) ? ' has-stamp' : ''; ?>">
		<?php the_content(); ?>
		<div class="cl"></div>
		<?php if( !empty($gs_stamp) ) { ?>
			<div id="stamped"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/gs-stamp.jpg" alt="100% Genuine Slate" /></div>
		<?php } ?>
	</div>
	<?php

}
/**
 * Renders sku and upc meta to single product
 */
function harman_woo_product_sku_html() {
	global $product;

	?>
	<div class="product-meta">
		<?php if ( wc_product_sku_enabled() && ( $product->get_sku() || $product->is_type( 'variable' ) ) ) : ?>
			<span class="sku_wrapper meta_wrap"><?php esc_html_e( 'Item No:', 'woocommerce' ); ?> <span class="sku"><?php echo ( $sku = $product->get_sku() ) ? $sku : esc_html__( 'N/A', 'woocommerce' ); ?></span></span>
		<?php endif; ?>

	<?php
		$upc = get_post_meta( $product->get_id(), '_upc', true );
		if( !empty( $upc ) ) { ?>
			<span class="upc_wrapper meta_wrap"><?php esc_html_e( 'UPC:', 'woocommerce' ); ?> <span class="upc"><?php echo $upc; ?></span></span>
		<?php } ?>
	</div>

	<?php
}

/**
 * Save variation meta
 * @param $variation_id
 * @param $i
 */
function harman_woo_save_product_variation( $variation_id, $i ) {
    // Min. order amount
	update_post_meta( $variation_id, 'min_order', wc_clean($_POST['variable_min_order_'][$i] ) );

    // Pricing
    update_post_meta( $variation_id, '_us_price', wc_clean( $_POST['variable_us_price'][ $i ] ) );
    update_post_meta( $variation_id, '_us_sale_price', wc_clean( $_POST['variable_us_sale_price_'][ $i ] ) );
    update_post_meta( $variation_id, '_retail_regular_price', wc_clean( $_POST['variable_retail_regular_price_'][ $i ] ) );
    update_post_meta( $variation_id, '_retail_sale_price', wc_clean( $_POST['variable_retail_sale_price_'][ $i ] ) );
    update_post_meta( $variation_id, '_retail_us_price', wc_clean( $_POST['variable_retail_us_price_'][ $i ] ) );
    update_post_meta( $variation_id, '_retail_us_sale_price', wc_clean( $_POST['variable_retail_us_sale_price_'][ $i ] ) );
}

/**
 * Save the product custom fields
 * @param $post_id
 */
function harman_woo_process_product_meta( $post_id ) {

	// UPC
	$upc = isset($_POST['_upc']) ? wc_clean($_POST['_upc']) : null;
	update_post_meta( $post_id, '_upc', $upc );

	$gs_stamp = !empty( $_POST['_gs_stamp'] );
	update_post_meta( $post_id, '_gs_stamp', $gs_stamp );

	// Min. order amount
	update_post_meta( $post_id, 'min_order', wc_clean($_POST['min_order'] ) );

	// Pricing
    update_post_meta($post_id, '_us_price', wc_clean( $_POST['_us_price'] ));
    update_post_meta($post_id, '_us_sale_price', wc_clean( $_POST['_us_sale_price'] ));
    update_post_meta($post_id, '_retail_regular_price', wc_clean( $_POST['_retail_regular_price'] ));
    update_post_meta($post_id, '_retail_sale_price', wc_clean( $_POST['_retail_sale_price'] ));
    update_post_meta($post_id, '_retail_us_price', wc_clean( $_POST['_retail_us_price'] ));
    update_post_meta($post_id, '_retail_us_sale_price', wc_clean( $_POST['_retail_us_sale_price'] ));

}

/**
 * Show the price fields for variations
 * @param $loop
 * @param $variation_data
 * @param $variation
 */
function harman_woo_variation_options_pricing( $loop, $variation_data, $variation ) {

	// Get variation data
	$us_price = get_post_meta( $variation->ID, '_us_price', true );
	$us_sale_price = get_post_meta( $variation->ID, '_us_sale_price', true );
	$retail_regular_price = get_post_meta( $variation->ID, '_retail_regular_price', true );
	$retail_sale_price = get_post_meta( $variation->ID, '_retail_sale_price', true );
	$retail_us_price = get_post_meta( $variation->ID, '_retail_us_price', true );
	$retail_us_sale_price = get_post_meta( $variation->ID, '_retail_us_sale_price', true );
	$min_order = get_post_meta( $variation->ID, 'min_order', true );

	// Regular USD
	woocommerce_wp_text_input( array(
		'id'            => "variable_us_price_{$loop}",
		'name'          => "variable_us_price[{$loop}]",
		'value'         => wc_format_localized_price( $us_price ),
		'label'         => sprintf( __( 'Regular price - USD (%s)', 'woocommerce' ), get_woocommerce_currency_symbol() ),
		'data_type'     => 'price',
		'wrapper_class' => 'form-row form-row-first',
		'placeholder'   => __( 'Variation price (USD)', 'woocommerce' ),
	) );

	// Sale pricing USD
	woocommerce_wp_text_input( array(
		'id'            => "variable_us_sale_price_{$loop}",
		'name'          => "variable_us_sale_price_[{$loop}]",
		'value'         => wc_format_localized_price( $us_sale_price ),
		'label'         => sprintf( __( 'Sale price - USD (%s)', 'woocommerce' ), get_woocommerce_currency_symbol() ),
		'data_type'     => 'price',
		'wrapper_class' => 'form-row form-row-last',
		'placeholder'   => __( 'Variation sale price (USD)', 'woocommerce' ),
	) );

	// Retail pricing CAD
	woocommerce_wp_text_input( array(
		'id'            => "variable_retail_regular_price_{$loop}",
		'name'          => "variable_retail_regular_price_[{$loop}]",
		'value'         => wc_format_localized_price( $retail_regular_price ),
		'label'         => sprintf( __( 'Retail price (%s)', 'woocommerce' ), get_woocommerce_currency_symbol() ),
		'data_type'     => 'price',
		'wrapper_class' => 'form-row form-row-first',
		'placeholder'   => __( 'Variation retail price in CAD', 'woocommerce' ),
	) );

	// Retail sale pricing CAD
	woocommerce_wp_text_input( array(
		'id'            => "variable_retail_sale_price_{$loop}",
		'name'          => "variable_retail_sale_price_[{$loop}]",
		'value'         => wc_format_localized_price( $retail_sale_price ),
		'label'         => sprintf( __( 'Retail sale price (%s)', 'woocommerce' ), get_woocommerce_currency_symbol() ),
		'data_type'     => 'price',
		'wrapper_class' => 'form-row form-row-last',
		'placeholder'   => __( 'Variation sale price in CAD', 'woocommerce' ),
	) );

	// Retail pricing USD
	woocommerce_wp_text_input( array(
		'id'            => "variable_retail_us_price_{$loop}",
		'name'          => "variable_retail_us_price_[{$loop}]",
		'value'         => wc_format_localized_price( $retail_us_price ),
		'label'         => sprintf( __( 'Retail price - USD (%s)', 'woocommerce' ), get_woocommerce_currency_symbol() ),
		'data_type'     => 'price',
		'wrapper_class' => 'form-row form-row-first',
		'placeholder'   => __( 'Variation retail price in USD', 'woocommerce' ),
	) );

	// Retail sale pricing USD
	woocommerce_wp_text_input( array(
		'id'            => "variable_retail_us_sale_price_{$loop}",
		'name'          => "variable_retail_us_sale_price_[{$loop}]",
		'value'         => wc_format_localized_price( $retail_us_sale_price ),
		'label'         => sprintf( __( 'Retail sale price - USD (%s)', 'woocommerce' ), get_woocommerce_currency_symbol() ),
		'data_type'     => 'price',
		'wrapper_class' => 'form-row form-row-last',
		'placeholder'   => __( 'Variation sale price in USD', 'woocommerce' ),
	) );

	// Min. order amount
	woocommerce_wp_text_input(array(
		'type'          => 'number',
		'id'            => "variable_min_order_{$loop}",
		'name'          => "variable_min_order_[{$loop}]",
		'value'         => $min_order,
		'label'         => __('Min. order amount', 'woocommerce'),
		'wrapper_class' => 'form-row',
		'placeholder'   => __('1', 'woocommerce'),
	));

}

/**
 * Pricing options
 */
function harman_woo_product_options_pricing() {
	global $post;

	$us_price = get_post_meta( $post->ID, '_us_price', true );
	$us_sale_price = get_post_meta( $post->ID, '_us_sale_price', true );

	// US Pricing
	woocommerce_wp_text_input( array(
		'id'          => '_us_price',
		'value'       => $us_price,
		'desc_tip'       => 'true',
		'label'       => __( 'Regular Price - USD ', 'woocommerce' ) . ' (' . get_woocommerce_currency_symbol() . ')',
		'description' => __( 'Wholesale user pricing in USD', 'woocommerce' ),
	) );

	// US Sale pricing
	woocommerce_wp_text_input( array(
		'id'          => '_us_sale_price',
		'value'       => $us_sale_price,
		'desc_tip'       => 'true',
		'label'       => __( 'Sale Price - USD ', 'woocommerce' ) . ' (' . get_woocommerce_currency_symbol() . ')',
		'description' => __( 'Wholesale user sale pricing in USD', 'woocommerce' ),
	) );

	$retail_regular_price = get_post_meta( $post->ID, '_retail_regular_price', true );
	$retail_sale_price = get_post_meta( $post->ID, '_retail_sale_price', true );
	$retail_us_price = get_post_meta( $post->ID, '_retail_us_price', true );
	$retail_us_sale_price = get_post_meta( $post->ID, '_retail_us_sale_price', true );

	// Retail pricing CAD
	woocommerce_wp_text_input( array(
		'id'          => '_retail_regular_price',
		'value'       => $retail_regular_price,
		'desc_tip'       => 'true',
		'label'       => __( 'Retail Regular Price ', 'woocommerce' ) . ' (' . get_woocommerce_currency_symbol() . ')',
		'description' => __( 'Retail regular pricing', 'woocommerce' ),
	) );

	// Retail sale pricing CAD
	woocommerce_wp_text_input( array(
		'id'          => '_retail_sale_price',
		'value'       => $retail_sale_price,
		'desc_tip'       => 'true',
		'label'       => __( 'Retail Sale Price ', 'woocommerce' ) . ' (' . get_woocommerce_currency_symbol() . ')',
		'description' => __( 'Retail user sale pricing', 'woocommerce' ),
	) );

	// Retail pricing USD
	woocommerce_wp_text_input( array(
		'id'          => '_retail_us_price',
		'value'       => $retail_us_price,
		'desc_tip'       => 'true',
		'label'       => __( 'Retail Regular Price - US ', 'woocommerce' ) . ' (' . get_woocommerce_currency_symbol() . ')',
		'description' => __( 'Retail regular pricing in USD', 'woocommerce' ),
	) );

	// Retail sale pricing USD
	woocommerce_wp_text_input( array(
		'id'          => '_retail_us_sale_price',
		'value'       => $retail_us_sale_price,
		'desc_tip'       => 'true',
		'label'       => __( 'Retail Sale Price - USD ', 'woocommerce' ) . ' (' . get_woocommerce_currency_symbol() . ')',
		'description' => __( 'Retail user sale pricing in USD', 'woocommerce' ),
	) );

}

/**
 * Adds custom fields to product general fields
 */
function harman_woo_product_options_general_product_data() {
	global $post, $product_object;

	$upc = get_post_meta( $post->ID, '_upc', true );
	$gs_stamp = get_post_meta( $post->ID, '_gs_stamp', true );
	$min_order = get_post_meta( $post->ID, 'min_order', true );

	// UPC # field
	woocommerce_wp_text_input( array(
		'id'          => '_upc',
		'value'       => $upc,
		'desc_tip'       => 'true',
		'label'       => __( 'UPC #', 'woocommerce' ),
		'placeholder' => _x( 'UPC #', 'placeholder', 'woocommerce' ),
		'description' => __( 'UPC # for this Product', 'woocommerce' ),
	) );

	// Genuine slate checkbox
	woocommerce_wp_checkbox( array(
		'id'            => '_gs_stamp',
		'value'         => !empty( $gs_stamp ) ? 'yes' : 'no',
		'label'         => __( 'Genuine Slate stamp?', 'woocommerce' ),
		'description'   => __( 'Check this box to show the stamp on product page', 'woocommerce' ),
	) );

	if( !$product_object->is_type('variable') ) {
		// Min order amount
		woocommerce_wp_text_input(array(
			'id'          => 'min_order',
			'type'        => 'number',
			'value'       => $min_order,
			'desc_tip'    => 'true',
			'label'       => __('Min. order amount', 'woocommerce'),
			'placeholder' => _x('1', 'placeholder', 'woocommerce'),
			'description' => __('Min. order amount for this product.', 'woocommerce'),
		));
    }
}

/**
 * Filters the related products columns
 * @param $columns
 * @return int
 */
function harman_woo_related_products_columns( $columns ) {
	return 3;
}

/**
 * No product found wrap start
 */
function harman_woo_no_products_found_wrapper_start() {
	echo '<div>';
}

/**
 * Product list wrap end
 */
function harman_woo_no_products_found_wrapper_end() {
	echo '</div>';
}
/**
 * Wrap start for shop product cart
 */
function harman_woo_shop_loop_item_cart_wrapper_start() {
	echo '<div class="product-cart"><a href="'. esc_url(get_permalink()) .'" title="View Product">View Product <i class="fa fa-long-arrow-right"></i></a>';
}

/**
 * Wrap end for shop product cart
 */
function harman_woo_shop_loop_item_cart_wrapper_end() {
	echo '</div>';
}

/**
 * Wrap start for product
 */
function harman_woo_shop_loop_item_wrapper_start() {
	echo '<div class="product-info"><div>';
}

/**
 * Wrap closing div for shop product
 */
function harman_woo_shop_loop_item_wrapper_end() {
	echo '</div></div>';
}

/**
 * Adds product holder wrap
 */
function harman_woo_product_wrapper_start() {
	echo '<div class="products-holder">';
}

/**
 * Adds product holder closing wrap
 */
function harman_woo_product_wrapper_end() {
	echo '</div></div>';
}

/**
 * Adds the wrapper opening divs
 */
function harman_wrapper_start() {
	echo '<section id="woo-wrap"><div class="container"><div class="product-wrap">';
}

/**
 * Adds the nav opening wrapper
 */
function harman_nav_wrapper_start() {
	if( is_archive() )
		echo '<div class="nav-wrap">';
}

/**
 * Adds the nav wrapper end
 */
function harman_nav_wrapper_end() {
	if( is_archive() )
		echo '</div>';
}

/**
 * Adds the wrapper closing divs
 */
function harman_wrapper_end() {
	echo '</div></div></section>';
}

/**
 * Echoes the category sidebar html for no products
 */
function harman_woo_no_product_categories_html() {
	if( is_archive() )
		echo harman_get_categories_html();
}

/**
 * Add categories html to product archive
 * @param $start_html
 * @return string
 */
function harman_woo_add_categories_html( $start_html ) {
	if( is_archive() ) {
		// Add categories
		$categories = harman_get_categories_html();
		return $categories . $start_html;
	}

	// Return
	return $start_html;
}

/**
 * Returns bool if user has previous order
 * @param int $user_id
 * @return bool
 */
function harman_user_has_bought( $user_id = 0 ) {
    global $wpdb;
    $customer_id = $user_id == 0 ? get_current_user_id() : $user_id;
    $paid_order_statuses = array_map( 'esc_sql', wc_get_is_paid_statuses() );

    $results = $wpdb->get_col( "
        SELECT p.ID FROM {$wpdb->prefix}posts AS p
        INNER JOIN {$wpdb->prefix}postmeta AS pm ON p.ID = pm.post_id
        WHERE p.post_status IN ( 'wc-" . implode( "','wc-", $paid_order_statuses ) . "' )
        AND p.post_type LIKE 'shop_order'
        AND pm.meta_key = '_customer_user'
        AND pm.meta_value = $customer_id LIMIT 1
    " );

    // Count number of orders and return a boolean value depending if higher than 0
    return count( $results ) > 0 ? true : false;
}

/**
 * Returns the categories list html
 * @return string
 */
function harman_get_categories_html() {
	ob_start();

	// Query the product categories
	$categories = harman_get_categories();

	$current = get_queried_object();
	$current_category_id = is_product_taxonomy() ? $current->term_id : 0;
	$is_current_cat = false;
	?>

		<div class="categories">
			<?php if( !empty($categories->get_terms()) ) { ?>
				<h2 class="category-header">Products</h2>
				<ul class="product-categories">
				<?php foreach($categories->get_terms() as $category) { ?>

                    <?php
				        $is_current_cat = $current_category_id == $category->term_id ? true : false;
				    ?>
					<li class="category<?php echo $is_current_cat ? ' active' : ''; ?>">
						<a href="<?php echo esc_url(get_term_link($category)); ?>" title="<?php echo esc_attr($category->name); ?>"><?php echo esc_html($category->name); ?></a>
						<?php $second_level = harman_get_categories($category->term_id); ?>
						<?php if( !empty($second_level->get_terms()) ) { ?>
							<ul class="sub" style="display: none;">
								<?php foreach($second_level->get_terms() as $category) { ?>
									<?php
									$is_current_cat = $current_category_id == $category->term_id ? true : false;
									?>
                                    <li class="category<?php echo $is_current_cat ? ' active' : ''; ?>">
										<a href="<?php echo esc_url(get_term_link($category)); ?>" title="<?php echo esc_attr($category->name); ?>"><?php echo esc_html($category->name); ?></a>
										<?php $third_level = harman_get_categories($category->term_id); ?>
										<?php if( !empty($third_level->get_terms()) ) { ?>
											<ul class="sub" style="display: none;">
												<?php foreach($third_level->get_terms() as $category) { ?>
													<?php
													$is_current_cat = $current_category_id == $category->term_id ? true : false;
													?>
                                                    <li class="category<?php echo $is_current_cat ? ' active' : ''; ?>">
														<a href="<?php echo esc_url(get_term_link($category)); ?>" title="<?php echo esc_attr($category->name); ?>"><?php echo esc_html($category->name); ?></a>
													</li>
												<?php } ?>
											</ul>
										<?php } ?>
									</li>
								<?php } ?>
							</ul>
						<?php } ?>
					</li>
				<?php } ?>
				</ul>
			<?php } ?>
		</div>
		<!-- Product wrap -->
		<div class="list">

	<?php
	$html = ob_get_clean();
	return $html;
}

/**
 * Returns the categories
 * @param int $parent   The parent term id
 * @return array|int|WP_Error
 */
function harman_get_categories($parent = 0) {

	$args = array(
		'hide_empty' => false,
		'orderby'    => 'name',
		'order'      => 'DESC',
		'taxonomy'   => 'product_cat',
		'parent'     => $parent,
		'exclude'    => array('1584'), // Uncategorized Term ID
	);

	$categories = new WP_Term_Query($args);
	return $categories;
}

/**
 * Filters the breadcrumb markup
 * @param $args
 * @return mixed
 */
function harman_woo_breadcrumb_defaults($args) {
	$args['delimiter'] = '<span class="delimiter">-</span>';
	return $args;
}

/**
 * Checks and auto detects the user currency using location
 */
function harman_woo_autodetect_currency() {
	// Not for ajax
	if ( is_ajax() ) {
		return;
	}

	// Get available currencies
	$currencies = harman_woo_get_currencies();

	// Check change currency
	if ( isset( $_GET['hm-currency'] ) && in_array( $_GET['hm-currency'], $currencies ) ) {
		if ( is_admin() ) {
			return;
		}
		$current_currency = $_GET['hm-currency'];
		// Set current
		harman_woo_set_current_currency( $current_currency );
		// Remove the arg
		wp_redirect( remove_query_arg('hm-currency') );
		exit;
	}

	// Auto select currency
	if ( isset( $_COOKIE['hm_current_currency'] ) ) {
		// Return if selected already
		return;
	} else {

		// Get currency info
		$detect_ip_currency = harman_woo_detect_ip_currency();

		if( isset( $detect_ip_currency['currency_code'] ) && in_array( $detect_ip_currency['currency_code'], $currencies ) ) {
			// Set currency
			harman_woo_set_current_currency( $detect_ip_currency['currency_code'] );
		} else if( isset( $detect_ip_currency['country_code'] ) && $detect_ip_currency['country_code'] ) {

			// Check depending on country code
			foreach( $currencies as $currency ) {
				$currency_detected = '';
				// Check by country
				$data = harman_woo_get_currency_by_countries($currency);
				if( in_array( $detect_ip_currency['country_code'], $data ) ) {
					$currency_detected = $currency;
					break;
				}
			}

			// Not found, revert back
			if ( !$currency_detected ) {
				$currency_detected = $detect_ip_currency['currency_code'];
			}

			// Set
			harman_woo_set_current_currency( $currency_detected );

		} else {
			// Default
			$default = 'CAD';
			harman_woo_set_current_currency( $default );
		}

	}
}

/**
 * Get client info as current country and currency code
 * @return array|mixed|string
 */
function harman_woo_detect_ip_currency() {

	if ( isset( $_COOKIE['hm_ip_info'] ) && $_COOKIE['hm_ip_info'] ) {
		$geoplugin_arg = json_decode( base64_decode( $_COOKIE['hm_ip_info'] ), true );
	} else {

	    // Use Woocommerce GeoLocation
		$ip            = new WC_Geolocation();
		$geo_ip        = $ip->geolocate_ip();
		$country_code  = isset( $geo_ip['country'] ) ? $geo_ip['country'] : '';

		// Check for country and block China, Hong Kong and India
		if( $country_code && in_array(strtoupper($country_code), array('IN', 'CN', 'HK')) ) {
			// Stop Executing
			die();
		}

		$geoplugin_arg = array(
			'country'       => $country_code,
			'currency_code' => harman_woo_get_currency_code( $country_code )
		);

		if ( $geoplugin_arg['country'] ) {
			setcookie( 'hm_ip_info', base64_encode( json_encode( $geoplugin_arg ) ), time() + 60 * 60 * 24, '/' );
			$_COOKIE['hm_ip_info'] = base64_encode( json_encode( $geoplugin_arg ) );
		} else {
			return array();
		}
	}

	// Auto select currency
	if ( is_array( $geoplugin_arg ) and isset( $geoplugin_arg['currency_code'] ) ) {
		return array(
			'currency_code' => $geoplugin_arg['currency_code'],
			'country_code'  => $geoplugin_arg['country']
		);
	}

}

/**
 * Returns the currency code using country code
 * @param $country_code
 * @return bool|mixed
 */
function harman_woo_get_currency_code( $country_code ) {
	if ( ! $country_code ) {
		return false;
	}

	// Codes
	$codes = array(
		'US' => 'USD',
		'CA' => 'CAD'
	);

	// Return
	return $codes[$country_code];
}

/**
 * Returns the currency array by country
 * @param $currency_code
 * @return array|mixed
 */
function harman_woo_get_currency_by_countries( $currency_code ) {
	// Countries
	$countries = array(
		'USD' => array('US'),
		'CAD' => array('CA')
	);
	// Return
	return $countries[$currency_code] ? $countries[$currency_code] : array();
}

/**
 * Returns the store currencies
 * @return array
 */
function harman_woo_get_currencies() {
    // Currencies codes
    $currencies = array(
        'US'    => 'USD',
        'CA'    => 'CAD'
    );
    return $currencies;
}

/**
 * Sets the currency currency
 * @param $currency_code
 */
function harman_woo_set_current_currency( $currency_code ) {
	if ( $currency_code ) {
		setcookie( 'hm_current_currency', $currency_code, time() + 60 * 60 * 24, '/' );
		$_COOKIE['hm_current_currency'] = $currency_code;
	}
}

/**
 * Get current currency
 * @return mixed
 */
function harman_woo_get_current_currency() {
	// Check currency
	$currencies = harman_woo_get_currencies();
	if ( isset( $_GET['hm-currency'] ) && in_array( $_GET['hm-currency'], $currencies ) ) {
		$current_currency = $_GET['hm-currency'];
	} elseif ( isset( $_COOKIE['hm_current_currency'] ) && in_array( $_COOKIE['hm_current_currency'], $currencies ) ) {
		$current_currency = $_COOKIE['hm_current_currency'];
	} else {
	    // Default is CAD
		$current_currency = 'CAD';
	}
    // Return
	return $current_currency;
}

/**
 * Checks whether the min quantity rule applies to current user
 * @return bool
 */
function harman_woo_min_quantity_rule_applies() {

	// Only for logged in users
	if( !is_user_logged_in() ) {
		return false;
	}

	// Check for wholesale users
	$user = wp_get_current_user();
	if( !in_array('wholesale', $user->roles) && !in_array('administrator', $user->roles) ) {
		return false;
	}

	return true;
}

/**
 * Localize woo variables
 * @param $handle
 */
function harman_woo_localize_vars($handle) {
}
jQuery(document).ready(function ($) {

    // Defaults
    var elem = "#rotator ul";
    var fadeInterval = 1000;
    var rotatorInterval = 8000;

    // Only on load
    $(window).on("load", function() {
        startBanner(elem);
    });

    /**
     * Rotate the banner
     * @param elem
     */
    function rotateBanner(elem) {

        var active = $(elem+ " li.active");
        var next = active.next();
        if( next.length == 0 ) {
            next = $(elem+" li:first");
        }

        active.removeClass("active").fadeOut(fadeInterval);
        next.addClass("active").fadeIn(fadeInterval);

    }

    /**
     * Prev Slide
     * @param elem
     */
    function prev(elem) {
        var active = $(elem+ " li.active");
        var prev = active.prev();
        if( prev.length == 0 ) {
            prev = $(elem+" li:last");
        }

        active.removeClass("active").fadeOut(fadeInterval);
        prev.addClass("active").fadeIn(fadeInterval);
    }

    /**
     * Next Slide
     * @param elem
     */
    function next(elem) {

        var active = $(elem+ " li.active");
        var next = active.next();
        if( next.length == 0 ) {
            next = $(elem+" li:first");
        }

        active.removeClass("active").fadeOut(fadeInterval);
        next.addClass("active").fadeIn(fadeInterval);

    }

    /**
     * Show a specific item
     * @param el
     * @param elem
     */
    function show(el, elem) {
        var id = el.data('item');
        var active = $(elem+ " li.active");

        if( id.length > 0 ) {
            active.removeClass("active").fadeOut(fadeInterval);
            $(elem+" li#"+id).addClass("active").fadeIn(fadeInterval);
        }
    }

    /**
     * Prepare
     * @param elem
     */
    function prepareBanner(elem) {
        $(elem+" li").fadeOut(0);
        $(elem+" li:first").fadeIn(0).addClass("active");
    }

    /**
     * Start the rotator
     * @param elem
     */
    function startBanner(elem) {
        prepareBanner(elem);
        setInterval(function(){rotateBanner(elem)}, rotatorInterval);
    }

    /**
     * Prev click
     */
    $("a.control_prev").on("click", function(ev) {
        ev.preventDefault();
        prev(elem);
    })

    /**
     * Next click
     */
    $("a.control_next").on("click", function(ev) {
        ev.preventDefault();
        prev(elem);
    })

    /**
     * Number nav click
     */
    $("a.rNav").on("click", function(ev) {
        ev.preventDefault();
        show($(this), elem);
    });

});
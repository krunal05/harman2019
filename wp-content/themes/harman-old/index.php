<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div id="blog-wrap">

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			if ( has_post_thumbnail() ) :
				$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
				$thumbnail = $thumbnail[0];
			else:
				$thumbnail = get_template_directory_uri() . '/assets/images/woods.jpg';
			endif;
			?>

            <section id="banner" style="background-image: url(<?php echo esc_attr($thumbnail); ?>);">
                <div class="wrapper">
                    <div class="banner-title">
                        <?php if ( is_home() && ! is_front_page() ) : ?>
                            <h1 class="page-title"><?php single_post_title(); ?></h1>
                    <?php else:  ?>
                            <h1 class="page-title"><?php _e( 'Events', 'harman' ); ?></h1>
                    <?php endif; ?>
                    </div>
                </div>
            </section>

            <section class="internal-panel">
                <div class="wrapper">
                    <div class="internal-panel-wrap">

                        <?php
                        if ( have_posts() ) :

                            /* Start the Loop */
                            while ( have_posts() ) : the_post();

                                /*
                                 * Include the Post-Format-specific template for the content.
                                 * If you want to override this in a child theme, then include a file
                                 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                                 */
                                get_template_part( 'template-parts/post/content', get_post_format() );

                            endwhile;

                        else :

                            get_template_part( 'template-parts/post/content', 'none' );

                        endif;
                        ?>
                    </div>

                    <div class="pagination">
	                    <?php
	                    if( function_exists('wp_pagenavi') ) {
		                    wp_pagenavi();
	                    }
	                    ?>
                    </div>

                </div>
            </section>

		</main><!-- #main -->
	</div><!-- #primary -->
</div>
<?php get_footer();

<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
    <?php
    $site_name = get_bloginfo('name');
    ?>

    <div id="header-top">
        <div class="container">
            <div class="wrap">
                <div class="location">
                    <span>Select Location</span>
                    <?php
                        $selected_currency = harman_woo_get_current_currency();
                        $select_cad_url = $selected_currency !== "CAD" ? add_query_arg('hm-currency', 'CAD') : '#';
                        $select_usd_url = $selected_currency !== "USD" ? add_query_arg('hm-currency', 'USD') : '#';
                    ?>
                    <a href="<?php echo $select_usd_url; ?>" title="Switch to USD" class="<?php echo $selected_currency === 'USD' ? 'active' : ''; ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/us-flag.jpg" alt="USD" /></a>
                    <a href="<?php echo $select_cad_url; ?>" title="Switch to CAD" class="<?php echo $selected_currency === 'CAD' ? 'active' : ''; ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/cdn-flag.jpg" alt="CAD" /></a>
                </div>
                <div class="members">

                    <?php
                        $dashboard_url = wc_get_page_permalink('myaccount');
                    ?>
                    <?php if( !is_user_logged_in() ) { ?>
                        <ul>
                            <li><a href="<?php echo $dashboard_url; ?>" title="Login">Login</a></li>
                            <li><a href="<?php echo get_bloginfo('url'); ?>/register" title="Register">Register</a></li>
                        </ul>
                    <?php } else {
                        global $current_user;
                        ?>
                        <span><a href="<?php echo $dashboard_url; ?>" title="View Dashboard">Welcome <?php echo esc_html($current_user->display_name); ?></a></span>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
	<header id="masthead" class="site-header" role="banner">

        <div class="container">
            <div class="wrap">

                <div class="left">
                    <div class="logo">
                        <a href="<?php echo get_bloginfo('url'); ?>" title="<?php echo $site_name; ?>">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.jpg" alt="<?php echo $site_name; ?>" />
                        </a>
                    </div>
                    <div class="navigation-top">
		                <?php get_template_part( 'template-parts/navigation/navigation', 'top' ); ?>
                    </div>
                </div>
                <div class="right">
                    <div class="product-icon" id="product-icon">
                        <span class="product-tag">PRODUCTS</span>
                        <!-- <span class="fa fa-th-list" id="product-icon-open"></span>-->
                    </div>
                    <div id="search-box">
                        <?php echo get_search_form(); ?>
                        <span id="search-close" class="fa fa-times"></span>
                    </div>
                    <div class="search">
                        <span class="fa fa-search" id="search-open"></span>
                    </div>
                    <div class="cart">
                        <a href="<?php echo wc_get_page_permalink('cart'); ?>" title="View Cart"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a>
                    </div>
                    <span id="device-nav-holder"><i class="fa fa-bars" aria-hidden="true"></i></span>
                    <div id="device-navigation">
		                <?php get_template_part( 'template-parts/navigation/navigation', 'top' ); ?>
                    </div>
                </div>

            </div>
        </div>



        <div class="container product-menu">
            <div class="wrap product-menu-wrap">
                <div class="left">
                    <div class="navigation-top navigation-product-top">

                        <ul id="menu-product-menu" class="menu">
                        </ul>

                    </div>
                </div>
            </div>
        </div>


	</header><!-- #masthead -->

	<div class="site-content-contain">
		<div id="content" class="site-content">

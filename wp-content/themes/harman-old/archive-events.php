<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

	<div id="page-wrap">
		<div id="primary" class="content-area">
			<main id="main" class="site-main" role="main">

				<section class="archive-events" id="archive">
					<div class="container">
						<div class="wrap">
							<div class="archive-posts">
								<h1 class="archive-title">Events</h1>
								<ul class="posts">
									<?php
									if ( have_posts() ) : ?>
										<?php
										/* Start the Loop */
										while ( have_posts() ) : the_post();

											/*
											 * Include the Post-Format-specific template for the content.
											 * If you want to override this in a child theme, then include a file
											 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
											 */
											get_template_part( 'template-parts/post/content', 'event' );

										endwhile;

									else :

										get_template_part( 'template-parts/post/content', 'none' );

									endif; ?>
								</ul>
								<?php
									if( function_exists('wp_pagenavi') ) {
										wp_pagenavi();
									}
								?>
							</div>
						</div>
					</div>
				</section>

			</main><!-- #main -->
		</div><!-- #primary -->
	</div>

<?php get_footer();

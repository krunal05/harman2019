<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

$copyright_text = "&copy; 2018 Harman. All Rights Reserved.";
$site_name = get_bloginfo('name');
?>

		</div><!-- #content -->

		<footer id="colophon" class="site-footer" role="contentinfo">
            <div class="bottom">
                <div class="container">
                    <div class="wrap">

                        <div class="left">
                            <div class="nav-items">
                                <div class="bottom-nav nav">
                                    <h3 class="title">Harman Inc.</h3>
	                                <?php wp_nav_menu( array(
		                                'theme_location' => 'footer',
		                                'menu_id'        => '',
		                                'container'      => false,
	                                ) ); ?>
                                </div>
                                <div class="middle-nav nav">
                                    <h3 class="title">Account</h3>
                                    <ul>
                                        <li><a href="<?php echo wc_get_page_permalink('myaccount'); ?>" title="Login">Login</a></li>
                                        <li><a href="<?php echo get_bloginfo('url'); ?>/register" title="Register">Register</a></li>
                                        <li><a href="<?php echo wc_get_page_permalink('cart'); ?>" title="View Cart">View Cart</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="contact-info">
	                            <?php dynamic_sidebar( 'footer-contact-1' ); ?>
                            </div>
                        </div>
                        <div class="right">
                            <h3 class="title">Connect with Us</h3>
                            <ul>
                                <li><a href="https://www.facebook.com/Harman-Inc-187783488444006/" title="Checkout our Facebook!" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/facebook.jpg" alt="Facebook"/></a></li>
                                <li><a href="https://twitter.com/harman_inc" title="Tweet to Us!" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/twitter.jpg" alt="Twitter"/></a></li>
                                <li><a href="https://www.instagram.com/harmaninc/" title="Checkout our Instagram!" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/instagram.jpg" alt="Instagram"/></a></li>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
            <div class="copyright">
                <div class="container">
                    <p><?php echo $copyright_text; ?></p>
                    <p class="web-design">
                        Website by GeekPower <a href="http://www.in-toronto-web-design.ca/" title="Web Design in Toronto" target="_blank">Web Design in Toronto</a>
                    </p>
                </div>
            </div>
		</footer><!-- #colophon -->

	</div><!-- .site-content-contain -->
</div><!-- #page -->


<?php

//$args = array(
//    'post_type' => array( 'product', 'product_variation'),
//    'fields'          => 'ids',
//    'post_status' => 'publish',
//    'posts_per_page' => -1
//);
//$products = get_posts( $args );
//
//foreach ($products as $prod_id){
//    update_post_meta( $prod_id, '_stock_status', 'instock');
////update_post_meta( $post_id, '_manage_stock', 'yes' );
//    update_post_meta( $prod_id, '_backorders', 'notify' );
//}



?>

<?php wp_footer(); ?>
</body>
</html>
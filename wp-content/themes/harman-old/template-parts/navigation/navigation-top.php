<?php
/**
 * Displays top navigation
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>
<nav class="main-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Top Menu', 'harman' ); ?>">
	<?php wp_nav_menu( array(
		'theme_location' => 'top',
		'menu_id'        => '',
		'container'      => false,
	) ); ?>
</nav><!-- #site-navigation -->

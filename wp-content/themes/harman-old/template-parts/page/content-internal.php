<?php
/**
 * Template part for displaying page content in internal page template
 */
?>
<div <?php post_class(); ?>>
    <section class="internal-panel">
        <div class="container">
            <div class="internal-panel-wrap">
                <div class="internal-content">
	                <?php the_content(); ?>
                </div>
            </div>
        </div>
    </section>
</div>

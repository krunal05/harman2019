<?php
/**
 * Template part for displaying sections on front page
 */
?>
<section id="banner">
    <div class="container">
        <div class="wrap">

            <div id="rotator">
                <?php
                    $rItems = gp_get_posts( array('post_type' => 'rotator', 'posts_per_page' => 8) );
                    $items = $numbers = '';
                    $counter = 0;
                    $template = get_template_directory_uri();
                    if( $rItems->have_posts() ) {
                        while($rItems->have_posts()) {
	                        $rItems->the_post();
	                        $counter++;

	                        $item_class = $counter === 1 ? ' active' : '';
	                        $style = $counter > 1 ? ' style="display:none;"' : '';
                            $items .= '<li class="item'.$item_class.'" id="rItem-'. $counter .'"'.$style.'>';
                                $thumbnail_img = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full');
                                $thumbnail_src = $thumbnail_img && isset( $thumbnail_img[0] ) ? $thumbnail_img[0] : $template . '/assets/images/banner-placeholder.jpg';
                                $items .= '<div class="item-thumb" style="background-image: url('. $thumbnail_src .');"></div>';
                                $items .= '<div class="item-shade"></div>';
	                            $items .= '<div class="item-content">';
                                    $items .= get_the_content();
                                $items .= '</div>';
                            $items .= '</li>';

                            $numbers .= '<a href="#" title="'. $counter .'" class="rNav" data-item="rItem-'. $counter .'">0'. $counter .'</a>';
                        }
                    }
                    wp_reset_postdata();
                ?>
                <ul class="items">
	                <?php echo $items; ?>
                </ul>
                <div class="rotator-number-nav">
                    <?php echo $numbers; ?>
                </div>
                <div class="rotator-nav">
                    <a href="#" class="control_prev" title="Previous"><i class="fa fa-chevron-left"></i></a>
                    <a href="#" class="control_next" title="Next"><i class="fa fa-chevron-right"></i></a>
                </div>
                <div class="scroll-below">
                    <a href="#featured-products" title="Scroll to featured products"><i class="fa fa-chevron-down"></i></a>
                </div>
            </div>

        </div>
    </div>
</section>

<?php
    $featured_args = array(
        'post_type'      => 'product',
        'posts_per_page' => 5,
        'post__in'       => wc_get_featured_product_ids(),
        'orderby'        => 'menu_order',
        'order'          => 'DESC'
    );
    $products = new WP_Query($featured_args);
    if( $products->have_posts() ) { ?>
        <section id="featured-products">
            <div class="container">

                <h2 class="feat-title">Featured Collection</h2>
                <div class="wrap">
                    <ul class="products">
                        <?php $counter = 0; ?>
                        <?php while( $products->have_posts() ) { $products->the_post(); ?>
                            <?php
                                global $product;
                                $counter++;
                                $classes = '';
                                if( $counter == 1 ) {
                                    $classes = ' first';
                                } else if( $counter == 2 ) {
                                    $classes = ' second';
                                }
                            ?>
                            <li class="product<?php echo $classes; ?>">
                                <?php
                                $size = $counter == 1 ? 'full' : 'woocommerce_thumbnail';
                                if ( has_post_thumbnail( $product->get_id() ) ) {
                                    $image_id = get_post_thumbnail_id($product->get_id());
                                    $image = wp_get_attachment_image_src($image_id, $size);
                                    $image_src = isset( $image[0] ) ? $image[0] : wc_placeholder_img_src();
                                } else {
                                    $image_src = wc_placeholder_img_src();
                                }
                                ?>

                                <div class="thumbnail" style="background-image: url(<?php echo esc_attr($image_src); ?>);"></div>
                                <div class="product-info">
                                    <div>
                                        <h2 class="woocommerce-loop-product__title"><a href="<?php echo get_permalink(); ?>" title="View"><?php echo get_the_title(); ?></h2></h2>
                                        <a class="view" href="<?php echo get_permalink(); ?>" title="View Product">View Product</a>
                                    </div>
                                </div>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </section>
    <?php } ?>
<?php wp_reset_postdata(); ?>

<?php
    if( has_post_thumbnail() ) {
        $home_thumbnail = $template . 'assets/images/catalogs-bg.jpg';
        $home_thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
        $home_thumbnail_src = $home_thumbnail[0];

        ?>
        <section id="catalogs">
            <div class="container">
                <div class="wrap" style="background-image: url(<?php echo $home_thumbnail_src; ?>);">

                    <div class="panel-content">
                        <?php the_content(); ?>
                    </div>

                </div>
            </div>
        </section>
        <?php
    }
?>

<section id="events-panel">
    <div class="container">

        <div class="events-holder">
            <h2 class="panel-title">Events</h2>
        </div>
        <div class="panel-holder">
            <?php
            $events = gp_get_posts( array('post_type' => 'events', 'posts_per_page' => 3, 'orderby' => 'post_date', 'order' => 'DESC') );
            if( $events->have_posts() ) {
                ?>
                <ul id="events">
                <?php
                    while( $events->have_posts() ) {
                        $events->the_post();
                        get_template_part( 'template-parts/post/content', 'event' );
                    }
                ?>
                </ul>
                <div class="archive-link">
                    <a href="<?php echo esc_url(get_post_type_archive_link('events')); ?>" title="View All">View All</a>
                </div>
                <?php
            }
            wp_reset_postdata();
            ?>
        </div>

    </div>
</section>

<?php
    $signup_panel_content = get_field('signup_panel');
    if( $signup_panel_content ) {
        $signup_panel_image = get_field('signup_bg');
        if( !$signup_panel_image ) {
            $signup_panel_image = $template . '/assets/images/signup-panel-bg.jpg';
        }
        ?>
        <section id="signup-panel">
            <div class="container">
                <div class="wrap" style="background-image: url(<?php echo $signup_panel_image; ?>);">

                    <div class="signup-content">
                        <?php echo $signup_panel_content; ?>
                    </div>

                </div>
            </div>
        </section>
        <?php
    }
?>
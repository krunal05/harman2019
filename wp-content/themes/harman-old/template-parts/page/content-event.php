<?php
/**
 * Template part for displaying content in event page
 */
?>
<div <?php post_class(); ?>>
	<section class="internal-panel">
		<div class="container">
			<div class="internal-panel-wrap">
				<div class="internal-content">
					<?php the_title('<h2 class="event-title">', '</h2>'); ?>
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</section>
</div>

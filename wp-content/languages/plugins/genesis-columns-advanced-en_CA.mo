��    /      �  C           	     	   #  	   -  	   7  	   A  E   K     �     �     �     �     �     �     �     �  
   �  
   �     
  
        "     4      B     c     p     }     �     �     �  
   �  a   �  %   5  -   [  -   �  .   �  .   �  -     -   C     q     w     �     �     �  	   �     �  %   �     �  :     2  >  	   q	  	   {	  	   �	  	   �	  	   �	  E   �	     �	     
     
     
     %
     .
     4
     @
  
   L
  
   W
     b
  
   o
     z
     �
      �
     �
     �
     �
     �
     �
     	  
      a   +  %   �  -   �  -   �  .     .   >  -   m  -   �     �     �     �     �     �  	          %        A  :   [                 &             .           ,   "         *         #          /         '      %          !                                            -                        $                  )   (   
      +   	                           2 Columns 3 Columns 4 Columns 5 Columns 6 Columns Adds shortcodes to easily create up to 42 different columned layouts. Advanced Column Layouts Advanced Layouts All Layouts Cancel Clearfix Close Column Five Column Four Column One Column Six Column Three Column Two Columns Container Documentation Filter by the number of columns. Five Columns Four Columns Genesis Columns Advanced Hide Titles Insert Columns Insert Selected Layout Nick Diego Optionally, add custom CSS classes to each column. Multiple classes must be separated by a space. Place your columns in this container. Place your content for the fifth column here. Place your content for the first column here. Place your content for the fourth column here. Place your content for the second column here. Place your content for the sixth column here. Place your content for the third column here. Reset Show Titles Six Columns Three Columns Two Columns Utilities Vertical Spacer You forgot to select a column layout! https://www.nickdiego.com https://www.nickdiego.com/plugins/genesis-columns-advanced PO-Revision-Date: 2017-03-13 23:55:32+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/2.4.0-alpha
Language: en_CA
Project-Id-Version: Plugins - Genesis Columns Advanced - Stable (latest release)
 2 Columns 3 Columns 4 Columns 5 Columns 6 Columns Adds shortcodes to easily create up to 42 different columned layouts. Advanced Column Layouts Advanced Layouts All Layouts Cancel Clearfix Close Column Five Column Four Column One Column Six Column Three Column Two Columns Container Documentation Filter by the number of columns. Five Columns Four Columns Genesis Columns Advanced Hide Titles Insert Columns Insert Selected Layout Nick Diego Optionally, add custom CSS classes to each column. Multiple classes must be separated by a space. Place your columns in this container. Place your content for the fifth column here. Place your content for the first column here. Place your content for the fourth column here. Place your content for the second column here. Place your content for the sixth column here. Place your content for the third column here. Reset Show Titles Six Columns Three Columns Two Columns Utilities Vertical Spacer You forgot to select a column layout. https://www.nickdiego.com https://www.nickdiego.com/plugins/genesis-columns-advanced 
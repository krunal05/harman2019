<?php
// Should run every 19 minutes
include('wp-config.php');

class GP_Sync{

	/**
	 * The path to the current file.
	 * @var string
	 */
	protected $file = '';

	/**
	 * The dir name
	 * @var string
	 */
	protected $dir = 'pathways_stock_data';

	/**
	 * File Data
	 * @var int
	 */
	protected $start_pos = 0;
	protected $lines = 900;
	protected $percent_completed = 0;
	protected $skipped = 0;
	protected $failed = 0;
	protected $updated = 0;
	protected $skipped_ids = array();

	/**
	 * Updated product ids
	 * @var array
	 */
	protected $ids_updated = array();

	/**
	 * GP_Sync constructor.
	 */
	public function __construct()
	{

		// Get the latest file to import from
		$this->getLatestCSVFile($this->dir);

		if( !ini_get( 'safe_mode' ) )
			set_time_limit( 0 );

		// Start sync
		$this->sync();
	}

	/**
	 * Returns whether this user can run this file
	 * @return bool
	 */
	protected function can_run() {
		// Not logged in
		if( !is_user_logged_in() ) {
			return false;
		}

		// Admins only
		$current_user = wp_get_current_user();
		if( !in_array( 'administrator', $current_user->roles ) ) {
			return false;
		}

		// Default
		return true;
	}

	/**
	 * Sync the inventory
	 */
	function sync() {
		global $wpdb;

		$filename = $this->file;
		// No file found
		if( empty($filename) ) {
			// Maybe log this
			echo 'No file found';
			return;
		}

		// Build
		$file = $this->dir . '/' . $filename;

		// Include the file
		include_once( WC_ABSPATH . 'includes/admin/importers/class-wc-product-csv-importer-controller.php' );
		include_once( WC_ABSPATH . 'includes/import/class-wc-product-csv-importer.php' );

		// Build import
		$params = array(
			'delimiter'       => ',',
			'start_pos'      => $this->start_pos,
			'mapping'         => array(),
			'update_existing' => true,
			'lines'           => $this->lines,
			'parse'           => true,
		);

		// Map fields to csv
		$params['mapping']['from'][] = 'SKU';
		$params['mapping']['from'][] = 'WMS ON-HAND';
		$params['mapping']['to'][] = 'sku';
		$params['mapping']['to'][] = 'stock_quantity';

		// Import
		$importer = WC_Product_CSV_Importer_Controller::get_importer( $file, $params );
		$results = $importer->import();
		$this->percent_completed = $importer->get_percent_complete();

		// Add count
		$this->skipped += count($results['skipped']);
		$this->failed += count($results['failed']);
		$this->updated += count($results['updated']);
		if( is_array( $results['updated'] ) ) {
			$this->ids_updated = array_merge($this->ids_updated, $results['updated']);
		} else {
			$this->ids_updated[] = $results['updated'];
		}

//		if( is_array($results['skipped']) ) {
//			foreach($results['skipped'] as $s ) {
//				$this->skipped_ids[] = $s->get_error_data('woocommerce_product_importer_error');
//			}
//		}

		if ( 100 === $this->percent_completed ) {
			// Clear temp meta.
			$wpdb->delete($wpdb->postmeta, array('meta_key' => '_original_id'));
			$wpdb->query("
				DELETE {$wpdb->posts}, {$wpdb->postmeta}, {$wpdb->term_relationships}
				FROM {$wpdb->posts}
				LEFT JOIN {$wpdb->term_relationships} ON ( {$wpdb->posts}.ID = {$wpdb->term_relationships}.object_id )
				LEFT JOIN {$wpdb->postmeta} ON ( {$wpdb->posts}.ID = {$wpdb->postmeta}.post_id )
				LEFT JOIN {$wpdb->term_taxonomy} ON ( {$wpdb->term_taxonomy}.term_taxonomy_id = {$wpdb->term_relationships}.term_taxonomy_id )
				LEFT JOIN {$wpdb->terms} ON ( {$wpdb->terms}.term_id = {$wpdb->term_taxonomy}.term_id )
				WHERE {$wpdb->posts}.post_type IN ( 'product', 'product_variation' )
				AND {$wpdb->posts}.post_status = 'importing'
			");

			echo sprintf('Failed: %1$d; Updated: %2$d<br/>', $this->failed, $this->updated);
			echo sprintf('Percent Completed: %d', $this->percent_completed);
			exit;

		} else {

			if( $this->percent_completed > 100 ) {
				exit;
			}
			// Run the sync again
			$this->start_pos = $importer->get_file_position();
			self::sync();

		}

	}

	/**
	 * Returns the latest CSV file in a dir
	 * @param $dir
	 * @return string
	 */
	function getLatestCSVFile($dir) {

		// File exists
		if( !empty( $this->file ) ) {
			return $this->file;
		}

		$ext = 'CSV';
		// Time for latest file
		$latestTime = 0;
		// Name of latest file
		$latestFile = "";
		if( $dh = opendir($dir) ) {
			while( ($filename = readdir($dh)) !== false ) {
				// Skip meta
				if( $filename == '.' || $filename == '..' ) continue;
				// Only CSV
				if( pathinfo($filename, PATHINFO_EXTENSION) != $ext ) continue;
				// Get file modified time
				$fileTime = filemtime($dir . "/" . $filename);
				if( $fileTime > $latestTime ) {
					$latestTime = $fileTime;
					$latestFile = $filename;
				}
			}
		}

		// Update file
		$this->file = $latestFile;

	}

	// TODO
	function logError() {}

}
new GP_Sync();